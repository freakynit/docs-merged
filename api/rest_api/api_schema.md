#### Api Schema

WebEngage API details.

### General Notes

**API HOST**

Access to all the resources available via API is over ``HTTPS`` and accessed from the host ``api.webengage.com``

	    https://api.webengage.com/
		
**Response Format**

All the data is sent in a ``JSON`` or ``XML`` format. ``format`` parameter can be passed in the URL as ``json`` or ``xml``. Default value is ``json``. (Any parameters can be passed as an HTTP query string parameter in the API Urls.)

**Date Format**

All timestamps are returned in the following format:

	    yyyy-MM-ddTHH:mm:ssZ
		
		e.g. 2013-01-26T07:31+0000

**Response Container**

Every API Response will be wrapped in a main container called ``response``. It will have three properties namely ``status``, ``data`` and ``message``. ``status`` will have values either ``success`` or ``error`` depending upon the server response. ``message`` will be related text for the corresponding ``status``. ``data`` will contain the data for the requested resource. It could be [Feedback Data](#get-a-feedback), [Feedback Reply Data](#get-a-feedback-reply) etc.
```json
		{
			"response" : {
				"data" : {
					...
				},
				"message" : "success",
				"status" : "success"
			}
		}
```

### Authentication
WebEngage API follows "Bearer" Authentication Scheme for the API access.

**API KEY**

You can get your API key for an account from your dashboard. Every account manager for an account will have a different API Key. Access restriction for an API Key is driven by the access privileges of that account manager. You have to pass the API key in header, as shown below :

		curl -H "Authorization: bearer your_api_key" https://api.webengage.com/v1/feedback/has7e2 -d "format=xml"

### Errors

Following is the list of errors you may receive.


**Invalid Resource/Parameters**

Invalid Resource Id will result in : ``400 Bad Request``	    

	    {
			"response" : {
			"message" : "Invalid request",
			"status" : "error"
			}
		}

**Invalid URL**

Invalid URL will result in : ``404 Not Found``

	    {
			"response" : {
			"message" : "Invalid request",
			"status" : "error"
			}
		}

**Invalid Auth/Access**

Invalid authentication parameter will result in : ``401 Unauthorized``

	    {
			"response" : {
			"message" : "Authentication Required",
			"status" : "error"
			}
		}
