#### Feedback Api

WebEngage allows you to query for the feedback response generated via WebEngage Feedback Widget using the **FeedbackId**. It also lets you query replies for a perticular feedback thread or even a specific reply using it's **ReplyId**.

### Get a Feedback

Get feedback details by **FeedbackId**.

	GET /v1/feedback/<FeedbackId>

#### XML Response

		<?xml version="1.0"?>
		<response>
				<data class="feedback">
				    <id>has7e2</id>
				    <licenseCode>311c48b3</licenseCode>
				    <activity>
				        <pageUrl>http://webengage.net/publisher/feedback-configuration/fields/311c48b3</pageUrl>
				        <pageTitle>Feedback Configuration - WebEngage</pageTitle>
				        <ip>127.0.0.1</ip>
				        <city>Mumbai</city>
				        <country>India</country>
				        <browser>Firefox</browser>
				        <browserVersion>18</browserVersion>
				        <platform>Linux</platform>
				        <activityOn>2013-02-11T08:09+0000</activityOn>
				    </activity>
				    <fields>
				        <field id="1fcdjgf" type="default">
				            <label>Name</label>
				            <value class="name">
				                <text>John</text>
				            </value>
				        </field>
				        <field id="ah1ihjd" type="default">
				            <label>Email</label>
				            <value class="email">
				                <text>john@doe.com</text>
				            </value>
				        </field>
				        <field id="cci1868" type="default">
				            <label>Category</label>
				            <value class="category">
				                <text>Suggestion</text>
				            </value>
				        </field>
				        <field id="~78me196" type="default">
				            <label>Message</label>
				            <value class="message">
				                <text>Thank you very much for this awesome service!</text>
				            </value>
				        </field>
				        <field id="~5d68amb" type="default">
				            <label>Attach a screenshot of this page</label>
				            <value class="snapshot">
				                <thumbnailImageUrl></thumbnailImageUrl>
				            </value>
				        </field>
				        <field id="n283q0" type="custom">
				            <label>Enter your mobile number</label>
				            <value class="text">
				                <text>9988776655</text>
				            </value>
				        </field>
					<field id="16qfkqk" type="custom">
						<label>"How likely is it that you would recommend WebEngage to a colleague?"</label>
						<value class="score">
							<value>10</value>
						</value>
					</field>
				        <field id="pp3j84" type="custom">
				            <label>Your Bio</label>
				            <value class="text">
				                <text>I am an early beta user of this product.</text>
				            </value>
				        </field>
				        <field id="19jb68o" type="custom">
				            <label>Which countries you have been to?</label>
				            <value class="list">
				                <options>
				                    <option>US</option>
				                    <option>Mexico</option>
				                </options>
				            </value>
				        </field>
				        <field id="1cc6lks" type="custom">
				            <label>Rate our website</label>
				            <value class="matrix">
				                <entries>
				                    <entry>
				                        <row>Experience</row>
				                        <columns>
				                            <column>Good</column>
				                        </columns>
				                    </entry>
				                    <entry>
				                        <row>Content</row>
				                        <columns>
				                            <column>Good</column>
				                        </columns>
				                    </entry>
				                    <entry>
				                        <row>Design</row>
				                        <columns>
				                            <column>Poor</column>
				                        </columns>
				                    </entry>
				                </entries>
				            </value>
				        </field>
				        <field id="sht4k8" type="custom">
				            <label>Upload your resume</label>
				            <value class="file">
				                <fileName>android_sdk_developing.pdf</fileName>
				                <fileSize>1919972</fileSize>
				                <fileAccessUrl>https://api.webengage.com/v1/accounts/311c48b3/feedback-responses/ofq4jy/download</fileAccessUrl>
				                <fileType>pdf</fileType>
				            </value>
				        </field>
				        <field id="16qfkqk" type="custom">
				            <label>What is your favourite color?</label>
				            <value class="list">
				                <options>
				                    <option>Red</option>
				                </options>
				            </value>
				        </field>
				    </fields>
					<customData>
						<entries>
							<entry>
								<key>userName</key>
								<values>
									<value>john</value>
								</values>
							</entry>
							<entry>
								<key>gender</key>
								<values>
									<value>male</value>
								</values>
							</entry>
							<entry>
								<key>customerType</key>
								<values>
									<value>Gold</value>
								</values>
							</entry>
							<entry>
								<key>jsessionid</key>
								<values>
									<value>CDB300FEF898236FF9E5A181E468CA6BCD</value>
								</values>
							</entry>							
						</entries>
					</customData>						
				    <replies>
				        <reply replyType="ADMIN">
				            <id>8bea242d</id>
				            <repliedByEmail>mike@company.com</repliedByEmail>
				            <repliedByName>Mike</repliedByName>
				            <repliedOn>2013-02-11T08:27+0000</repliedOn>
				            <repliedText>Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.

		Regards,
		Mike</repliedText>
				            <attachments></attachments>
				        </reply>
				        <reply replyType="USER">
				            <id>a5828a9e</id>
				            <repliedByEmail>john@doe.com</repliedByEmail>
				            <repliedByName>john</repliedByName>
				            <repliedOn>2013-02-11T08:37+0000</repliedOn>
				            <repliedText>Thanks! looking forward for the release.</repliedText>
				            <attachments></attachments>
				        </reply>
				        <reply replyType="ADMIN">
				            <id>be1a0220</id>
				            <repliedByEmail>mike@company.com</repliedByEmail>
				            <repliedByName>Mike</repliedByName>
				            <repliedOn>2013-02-11T08:39+0000</repliedOn>
				            <repliedText>Sure. Meanwhile, take a look at the attached screenshot of the first draft. </repliedText>
				            <attachments>
				                <attachment>
				                    <fileName>git.from.bottom.up.pdf</fileName>
				                    <fileSize>811094</fileSize>
				                    <fileAccessUrl>https://api.webengage.com/v1/accounts/311c48b3/feedback-reply-attachments/76178a43a/download</fileAccessUrl>
				                    <fileType>pdf</fileType>
				                </attachment>
				            </attachments>
				        </reply>
				    </replies>
				</data>
				<message>success</message>
				<status>success</status>
		</response>

#### JSON Response

	{
			"response" : {
				"data" : {
				  "licenseCode" : "311c48b3",
				  "fields" : [ {
				    "id" : "1fcdjgf",
				    "label" : "Name",
				    "type" : "default",
				    "value" : {
				      "@class" : "name",
				      "text" : "John"
				    }
				  }, {
				    "id" : "ah1ihjd",
				    "label" : "Email",
				    "type" : "default",
				    "value" : {
				      "@class" : "email",
				      "text" : "john@doe.com"
				    }
				  }, {
				    "id" : "cci1868",
				    "label" : "Category",
				    "type" : "default",
				    "value" : {
				      "@class" : "category",
				      "text" : "Suggestion"
				    }
				  }, {
				    "id" : "~78me196",
				    "label" : "Message",
				    "type" : "default",
				    "value" : {
				      "@class" : "message",
				      "text" : "Thank you very much for this awesome service!"
				    }
				  }, {
				    "id" : "~5d68amb",
				    "label" : "Attach a screenshot of this page",
				    "type" : "default",
				    "value" : {
				      "@class" : "snapshot",
				      "thumbnailImageUrl" : ""
				    }
				  }, {
				    "id" : "n283q0",
				    "label" : "Enter your mobile number",
				    "type" : "custom",
				    "value" : {
				      "@class" : "text",
				      "text" : "9988776655"
				    }
				  }, {
				    "id" : "pp3j84",
				    "label" : "Your Bio",
				    "type" : "custom",
				    "value" : {
				      "@class" : "text",
				      "text" : "I am an early beta user of this product."
				    }
				  }, {
				    "id" : "19jb68o",
				    "label" : "Which countries you have been to?",
				    "type" : "custom",
				    "value" : {
				      "@class" : "list",
				      "values" : [ "US", "Mexico" ]
				    }
				  }, {
				    "id" : "1cc6lks",
				    "label" : "Rate our website",
				    "type" : "custom",
				    "value" : {
				      "@class" : "matrix",
				      "values" : {
				        "Experience" : [ "Good" ],
				        "Content" : [ "Good" ],
				        "Design" : [ "Poor" ]
				      }
				    }
				  }, {
				    "id" : "sht4k8",
				    "label" : "Upload your resume",
				    "type" : "custom",
				    "value" : {
				      "@class" : "file",
				      "fileName" : "android_sdk_developing.pdf",
				      "fileSize" : 1919972,
				      "fileAccessUrl" : "https://api.webengage.com/v1/accounts/311c48b3/feedback-responses/ofq4jy/download",
				      "fileType" : "pdf"
				    }
				  }, {
				    "id" : "16qfkqk",
				    "label" : "What is your favourite color?",
				    "type" : "custom",
				    "value" : {
				      "@class" : "list",
				      "values" : [ "Red" ]
				    }
				  },{
				    "id" : "16qhkqk",
				    "label" : "How likely is it that you would recommend WebEngage to a colleague?",
				    "type" : "custom",
				    "value" : {
				      "@class" : "score",
				      "value" : 10
				    }
			          } ],
				  "customData" : {
					  "userName" : [ "john" ],
					  "gender" : [ "male" ],
					  "customerType" : [ "Gold" ],
					  "jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
				  },				  
				  "replies" : [ {
				    "repliedByEmail" : "mike@company.com",
				    "repliedByName" : "Mike",
				    "repliedOn" : "2013-02-11T08:27+0000",
				    "repliedText" : "Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.\r\n\r\nRegards,\r\nMike",
				    "attachments" : [ ],
				    "replyType" : "ADMIN",
				    "id" : "8bea242d"
				  }, {
				    "repliedByEmail" : "john@doe.com",
				    "repliedByName" : "john",
				    "repliedOn" : "2013-02-11T08:37+0000",
				    "repliedText" : "Thanks! looking forward for the release.",
				    "attachments" : [ ],
				    "replyType" : "USER",
				    "id" : "a5828a9e"
				  }, {
				    "repliedByEmail" : "mike@company.com",
				    "repliedByName" : "Mike",
				    "repliedOn" : "2013-02-11T08:39+0000",
				    "repliedText" : "Sure. Meanwhile, take a look at the attached screenshot of the first draft. ",
				    "attachments" : [ {
				      "fileName" : "git.from.bottom.up.pdf",
				      "fileSize" : 811094,
				      "fileAccessUrl" : "https://api.webengage.com/v1/accounts/311c48b3/feedback-reply-attachments/76178a43a/download",
				      "fileType" : "pdf"
				    } ],
				    "replyType" : "ADMIN",
				    "id" : "be1a0220"
				  } ],
				  "id" : "has7e2",
				  "activity" : {
				    "pageUrl" : "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
				    "pageTitle" : "Feedback Configuration - WebEngage",
				    "ip" : "127.0.0.1",
				    "city" : "Mumbai",
				    "country" : "India",
				    "browser" : "Firefox",
				    "browserVersion" : "18",
				    "platform" : "Linux",
				    "activityOn" : "2013-02-11T08:09+0000"
				  }
				},
				"message" : "success",
				"status" : "success"
			}
		}

#### User Activity

Details of the user taking feedback on the publisher website using WebEngage Feedback.

	{
		"pageUrl" : "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
		"pageTitle" : "Feedback Configuration - WebEngage",
		"ip" : "127.0.0.1",
		"city" : "Mumbai",
		"country" : "India",
		"browser" : "Firefox",
		"browserVersion" : "18",
		"platform" : "Linux",
		"activityOn" : "2013-02-11T08:09+0000"
	}

#### Form Fields

All the responses for the **Form Fields** of the feedback form are inside ``fields`` container. ``id`` specifies the field-id and type could be ``default`` (default fields present in the WebEngage feedback form) or ``custom`` (custom fields added in the form)

##### Default Form Fields

There are 5 types of **Default Fields** namely ``name``, ``email``, ``category``, ``message`` and ``snapshot``. ``class`` indicates the type of **Default Field**. Example of each is listed below. 

**Name Field**

	    {
			"id" : "1fcdjgf",
			"label" : "Name",
			"type" : "default",
			"value" : {
				"@class" : "name",
				"text" : "John"
			}
		}

**Email Field**

	    {
			"id" : "ah1ihjd",
			"label" : "Email",
			"type" : "default",
			"value" : {
				"@class" : "email",
				"text" : "john@doe.com"
			}
		}

**Category Field**

	    {
			"id" : "cci1868",
			"label" : "Category",
			"type" : "default",
			"value" : {
				"@class" : "category",
				"text" : "Suggestion"
			}
		}

**Message Field**

	    {
			"id" : "~78me196",
			"label" : "Message",
			"type" : "default",
			"value" : {
				"@class" : "message",
				"text" : "Thank you very much for this awesome service!"
			}
		}

**Snapshot Field**

	    {
			"id" : "~5d68amb",
			"label" : "Attach a screenshot of this page",
			"type" : "default",
			"value" : {
				"@class" : "snapshot",
				"thumbnailImageUrl" : "http://s3-ap-southeast-1.amazonaws.com/wk-static-files/feedback/82617417/1358664682292-095c-ed2f-06bb-thumb-small.jpg",
				"fullImageUrl" : "http://s3-ap-southeast-1.amazonaws.com/wk-static-files/feedback/82617417/1358664682292-095c-ed2f-06bb-full.jpg"
			}
		}

*Note* : ``thumbnailImageUrl`` and ``fullImageUrl`` are the public urls of the screenshot of the page, on which feedback was submitted.


##### Custom Form Fields

There are 4 types of **Custom Fields** namely ``list``, ``matrix``, ``file`` and ``text``. ``class`` indicates the type of **Custom Field**. Example of each type is listed below.

**Text Field**

Simple text field

	    {
			"id" : "n283q0",
			"label" : "Enter your mobile number",
			"type" : "custom",
			"value" : {
				"@class" : "text",
				"text" : "9988776655"
			}
		}

Simple comment field

	    {
			"id" : "pp3j84",
			"label" : "Your Bio",
			"type" : "custom",
			"value" : {
				"@class" : "text",
				"text" : "I am an early beta user of this product."
			}
		}

**List Field**

Single Options (radio and single-select drop-down) Fields

        {
			"id" : "16qfkqk",
			"label" : "What is your favourite color?",
			"type" : "custom",
			"value" : {
				"@class" : "list",
				"values" : [ "Red" ]
			}
		} 

List of Multi Options (radio and multi-select drop-down) Fields

	    {
			"id" : "19jb68o",
			"label" : "Which countries you have been to?",
			"type" : "custom",
			"value" : {
				"@class" : "list",
				"values" : [ "US", "Mexico" ]
			}
		}

**Matrix Field**

Values for the matrix type questions.

	    {
			"id" : "1cc6lks",
			"label" : "Rate our website",
			"type" : "custom",
			"value" : {
				"@class" : "matrix",
				"values" : {
					"Experience" : [ "Good" ],
					"Content" : [ "Good" ],
					"Design" : [ "Poor" ]
				}
			}
		}

**File Field**

Values for the file uploads.

	    {
			"id" : "sht4k8",
			"label" : "Upload your resume",
			"type" : "custom",
			"value" : {
				"@class" : "file",
				"fileName" : "android_sdk_developing.pdf",
				"fileSize" : 1919972,
				"fileAccessUrl" : "https://api.webengage.com/v1/accounts/311c48b3/feedback-responses/ofq4jy/download",
				"fileType" : "pdf"
			}
		}

*Note* : Files can be downloaded by accessing the ``fileAccessUrl`` with api-credentials. Also you can make use of the ``fileName`` value to save the downloaded file like this

	curl -H "Authorization: bearer your_api_token" -o android_sdk_developing.pdf https://api.webengage.com/v1/accounts/311c48b3/feedback-responses/ofq4jy/download
	
	  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
		                             Dload  Upload   Total   Spent    Left  Speed
	100 73623    0 73623    0     0  1630k      0 --:--:-- --:--:-- --:--:-- 1711k	


#### Custom Data

Custom data fields that are submitted along with the feedback on the publisher site.

	{
        "userName" : [ "john" ],
		"gender" : [ "male" ],
        "customerType" : [ "Gold" ],
        "jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
    }

#### Replies

``replies`` holds the list of replies in a particular feedback thread. ``reply`` holds all the data related to a perticular reply including all the attachments. [Goto](#get-feedback-replies) to see more about feedback replies/reply.


### Get Feedback Replies

Get all feedback replies using **FeedbackId**


	GET /v1/feedback/<FeedbackId>/replies


#### JSON Response

First reply added from site owner (Mike).

		{
			"response" : {
					"data" :
					[ {
						  "repliedByEmail" : "mike@company.com",
						  "repliedByName" : "Mike",
						  "repliedOn" : "2013-02-11T08:27+0000",
						  "repliedText" : "Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.\r\n\r\nRegards,\r\nMike",
						  "attachments" : [ ],
						  "replyType" : "ADMIN",
						  "id" : "8bea242d"
					} ]
				}
		}

Subsequent reply from John gets appended in the same "replies"

			{
				"response" : {
						"data" :
					[ {
							"repliedByEmail" : "mike@company.com",
							"repliedByName" : "Mike",
							"repliedOn" : "2013-02-11T08:27+0000",
							"repliedText" : "Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.\r\n\r\nRegards,\r\nMike",
							"attachments" : [ ],
							"replyType" : "ADMIN",
							"id" : "8bea242d"
						}, {
							"repliedByEmail" : "john@doe.com",
							"repliedByName" : "john",
							"repliedOn" : "2013-02-11T08:37+0000",
							"repliedText" : "Thanks! looking forward for the release.",
							"attachments" : [ ],
							"replyType" : "USER",
							"id" : "a5828a9e"
						} ]
					}
			}

While replying, feedback manager or the user can add file attachments.

		{
			"response" : {
					"data" :
					[ {
								"repliedByEmail" : "mike@company.com",
								"repliedByName" : "Mike",
								"repliedOn" : "2013-02-11T08:39+0000",
								"repliedText" : "Sure. Meanwhile, take a look at the attached screenshot of the first draft. ",
								"attachments" : [ {
								  "fileName" : "pic-feautre-beta.jpg",
								  "fileSize" : 811094,
								  "fileAccessUrl" : "https://api.webengage.com/v1/accounts/311c48b3/feedback-reply-attachments/76178a43a/download",
								  "fileType" : "jpg"
								} ],
								"replyType" : "ADMIN",
								"id" : "be1a0220"
					} ]
				}
		}

*Note* : Attachments can be downloaded by accessing the ``fileAccessUrl`` with api-credentials. Also you can make use of the ``fileName`` value to save the downloaded file like this

	curl -H "Authorization: bearer your_api_token" -o pic-feautre-beta.jpg https://api.webengage.com/v1/accounts/311c48b3/feedback-reply-attachments/76178a43a/download
	
	  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
		                             Dload  Upload   Total   Spent    Left  Speed
	100 73623    0 73623    0     0  1630k      0 --:--:-- --:--:-- --:--:-- 1711k	


### Get a Feedback Reply

Get individual feedback reply using **ReplyId**

	GET /v1/feedback/<FeedbackId>/reply/<ReplyId>

#### JSON Response

	{
			"response" : {
				"data" : {
				  "repliedByEmail" : "mike@company.com",
				  "repliedByName" : "Mike",
				  "repliedOn" : "2013-02-11T08:27+0000",
				  "repliedText" : "Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.\r\n\r\nRegards,\r\nMike",
				  "attachments" : [ ],
				  "replyType" : "ADMIN",
				  "id" : "8bea242d"
				},
				"message" : "success",
				"status" : "success"
			}
		}
