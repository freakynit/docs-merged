#### Survey Api

WebEngage allows trusted partners to create a survey for an existing account and allows to query for the survey response generated via WebEngage Survey Widget using the **ResponseId**.

### Create a Survey

Create survey for a WebEngage account with licenseCode **LicenseCode**.

	/v1/survey/<LicenseCode>/create

#### Survey Create Input XML

	<?xml version="1.0" encoding="UTF-8"?>
	<request>
	   <data class="surveys">
	      <list>
	         <survey>
	            <title>first survey</title>
	            <description>descriptiom</description>
	            <startDate>2013-07-12T12:04:40+0000</startDate>
	            <endDate>2013-07-17T12:04:40+0000</endDate>
	            <responseChannel>widget</responseChannel>
	            <thanksMessage>Thank you for your time !</thanksMessage>
	            <questions>
	               <simpleChoiceQuestion>
	                  <text>Where do you live ?</text>
	                  <type>CHOICE</type>
	                  <mandatory>true</mandatory>
	                  <allowMultipleAnswers>true</allowMultipleAnswers>
	                  <addCommentBox>false</addCommentBox>
	                  <errorMessage>ERRRO</errorMessage>
	                  <choices>
	                     <string>place 1</string>
	                     <string>place 2</string>
	                     <string>place 3</string>
	                  </choices>
	               </simpleChoiceQuestion>
	               <simpleChoiceQuestion>
	                  <text>Your age range ?</text>
	                  <type>DROPDOWN</type>
	                  <mandatory>true</mandatory>
	                  <allowMultipleAnswers>false</allowMultipleAnswers>
	                  <addCommentBox>false</addCommentBox>
	                  <errorMessage>ERRRO!</errorMessage>
	                  <choices>
	                     <string>below 15</string>
	                     <string>between 15 and 40</string>
	                     <string>above 3</string>
	                  </choices>
	               </simpleChoiceQuestion>
	               <matrixChoiceQuestion>
	                  <text>Vehicles you user for the daily commute ?</text>
	                  <type>MATRIX_OF_CHOICE</type>
	                  <mandatory>false</mandatory>
	                  <allowMultipleAnswers>true</allowMultipleAnswers>
	                  <addCommentBox>true/addCommentBox>
	                  <errorMessage>ERRRO!</errorMessage>
	                  <rowChoices>
	                     <string>Private transport</string>
	                     <string>Local transport</string>
	                  </rowChoices>
	                  <columnChoices>
	                     <string>two wheeler</string>
	                     <string>car/taxi</string>
	                     <string>bus</string>
	                  </columnChoices>
	               </matrixChoiceQuestion>
	               <question>
	                  <text>Your name?</text>
	                  <type>TEXT</type>
	                  <mandatory>true</mandatory>
	                  <addCommentBox>false</addCommentBox>
	                  <errorMessage>ERRRO!</errorMessage>
	               </question>
	               <question>
	                  <text>Leave comment?</text>
	                  <type>COMMENT</type>
	                  <mandatory>true/false</mandatory>
	                  <errorMessage>ERRRO!</errorMessage>
	               </question>
	            </questions>
	         </survey>
	      </list>
	   </data>
	</request>
	
#### Survey Create Response XML

	<?xml version="1.0" encoding="UTF-8"?>
	 <response>    
	 	<message>Successfully created one survey for publisher domain johndeo.com</message>    
	 	<status>success</status>
	</response>

### Get a Survey Response

Get survey response details by **ResponseId**.

	GET /v1/survey/response/<ResponseId>

#### Survey XML Response

		<response>
			    <data class="surveyResponse">
				<id>1kfrn7c</id>
				<surveyId>7djl637</surveyId>
				<title>Customer Satisfaction Survey</title>
				<licenseCode>311c48b3</licenseCode>
				<activity>
				    <pageUrl>http://webengage.net/</pageUrl>
				    <pageTitle>In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage</pageTitle>
				    <ip>127.0.0.1</ip>
				    <city>Mumbai</city>
				    <country>India</country>
				    <browser>Chrome</browser>
				    <browserVersion>26</browserVersion>
				    <platform>Linux</platform>
				    <activityOn>2013-05-08T10:34:33+0000</activityOn>
				</activity>
				<questionResponses>
				    <questionResponse id="~11uth8m">
					<questionId>6f51d6</questionId>
					<questionText>Which countries you have been to?</questionText>
					<value class="list">
					    <options>
						<option>Mexico</option>
					    </options>
					</value>
				    </questionResponse>
				    <questionResponse id="~vqxl96">
					<questionId>980gpa</questionId>
					<questionText>Rate our website</questionText>
					<value class="matrix">
					    <entries>
						<entry>
						    <row>Experience</row>
						    <columns>
						        <column>Good</column>
						    </columns>
						</entry>
						<entry>
						    <row>Content</row>
						    <columns>
						        <column>Poor</column>
						    </columns>
						</entry>
						<entry>
						    <row>Design</row>
						    <columns>
						        <column>Good</column>
						    </columns>
						</entry>
					    </entries>
					</value>
				    </questionResponse>
				    <questionResponse id="~67j6bv">
					<questionId>2i98bsk</questionId>
					<questionText>Upload Product Image</questionText>
					<value class="file">
					    <fileName>shoes-for-men-by-tzaro.jpg</fileName>
					    <fileSize>33731</fileSize>
					    <fileAccessUrl>https://api.webengage.com/v1/accounts/311c48b3/survey-responses/~67j6bv/download</fileAccessUrl>
					    <fileType>jpg</fileType>
					</value>
				    </questionResponse>
				    <questionResponse id="~13nacf">
					<questionId>2l23rao</questionId>
					<questionText>What is your favourite color?</questionText>
					<value class="list">
					    <options>
						<option>Red</option>
						<option>Blue</option>
					    </options>
					</value>
				    </questionResponse>
				    <questionResponse id="ynhviv">
					<questionId>16qfkqk</questionId>
					<questionText>"How likely is it that you would recommend WebEngage to a colleague?"</questionText>
					<value class="score">
						<value>10</value>
					</value>
				    </questionResponse>
				    <questionResponse id="~gfaxas">
					<questionId>2notcms</questionId>
					<questionText>Your profile</questionText>
					<value class="map">
					    <entries>
						<entry>
						    <key>Name</key>
						    <value>jhon</value>
						</entry>
						<entry>
						    <key>Qualification</key>
						    <value/>
						</entry>
						<entry>
						    <key>Age</key>
						    <value>24</value>
						</entry>
					    </entries>
					</value>
				    </questionResponse>
				</questionResponses>
				<customData>
				    <entries>
					<entry>
					    <key>customerType</key>
					    <values>
						<value>Gold</value>
					    </values>
					</entry>
					<entry>
					    <key>productCategory</key>
					    <values>
						<value>Footwear</value>
					    </values>
					</entry>
					<entry>
					    <key>jsessionid</key>
					    <values>
						<value>CDB300FEF898236FF9E5A181E468CA6BCD</value>
					    </values>
					</entry>
				    </entries>
				</customData>
			    </data>
			    <message>success</message>
			    <status>success</status>
		</response>

#### Survey JSON Response

	{
		    "response": {
			"data": {
			    "surveyId": "7djl637",
			    "title": "Customer Satisfaction Survey",
			    "licenseCode": "311c48b3",
			    "questionResponses": [{
				    "id": "~11uth8m",
				    "questionId": "6f51d6",
				    "questionText": "Which countries you have been to?",
				    "value": {
				        "@class": "list",
				        "values": ["Mexico"]
				    }
				}, {
				    "id": "~vqxl96",
				    "questionId": "980gpa",
				    "questionText": "Rate our website",
				    "value": {
				        "@class": "matrix",
				        "values": {
				            "Experience": ["Good"],
				            "Content": ["Poor"],
				            "Design": ["Good"]
				        }
				    }
				}, {
				    "id": "~67j6bv",
				    "questionId": "2i98bsk",
				    "questionText": "Upload Product Image",
				    "value": {
				        "@class": "file",
				        "fileName": "shoes-for-men-by-tzaro.jpg",
				        "fileSize": 33731,
				        "fileAccessUrl": "https://api.webengage.com/v1/accounts/311c48b3/survey-responses/~67j6bv/download",
				        "fileType": "jpg"
				    }
				}, {
				    "id": "~13nacf",
				    "questionId": "2l23rao",
				    "questionText": "What is your favourite color?",
				    "value": {
				        "@class": "list",
				        "values": ["Red", "Blue"]
				    }
				}, {
				    "id": "~gfaxas",
				    "questionId": "2notcms",
				    "questionText": "Your profile",
				    "value": {
				        "@class": "map",
				        "values": {
				            "Name": "jhon",
				            "Qualification": "",
				            "Age": "24"
				        }
				    }
				}
			    ],
			    "customData": {
				"customerType": ["Gold"],
				"productCategory": ["Footwear"],
				"jsessionid": ["CDB300FEF898236FF9E5A181E468CA6BCD"]
			    },
			    "id": "1kfrn7c",
			    "activity": {
				"pageUrl": "http://webengage.net/",
				"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
				"ip": "127.0.0.1",
				"city": "Mumbai",
				"country": "India",
				"browser": "Chrome",
				"browserVersion": "26",
				"platform": "Linux",
				"activityOn": "2013-05-08T10:34:33+0000"
			    }
			},
			"message": "success",
			"status": "success"
		    }
	}
#### Survey User Activity

Details of the user taking feedback on the publisher website using WebEngage Feedback.

		{
		    "pageUrl": "http://webengage.net/",
		    "pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
		    "ip": "127.0.0.1",
		    "city": "Mumbai",
		    "country": "India",
		    "browser": "Chrome",
		    "browserVersion": "26",
		    "platform": "Linux",
		    "activityOn": "2013-05-08T10:34:33+0000"
		}

#### Survey Question Response

All the responses for the **survey question** are inside ``questionResponses`` container. ``id`` specifies the question_response_id.
There are 5 types of **question_type** namely ``list``, ``matrix``, ``file``, ``text`` and ``map``. ``class`` indicates the type of **question**. Example of each type is listed below.

**Text Question Response**

Simple text Question

	   	{
		    "id": "~gfaxas",
		    "questionId": "2notcms",
		    "questionText": "Your profile",
		    "value": {
			"@class": "map",
			"values": {
			    "Name": "jhon",
			    "Qualification": "",
			    "Age": "24"
			}
		    }
		}

**List Question Response**

Single Options (radio and single-select drop-down) Question

		{
			"id": "~11uth8m",
			"questionId": "6f51d6",
			"questionText": "Which countries you have been to?",
			"value": {
				"@class": "list",
				"values": ["Mexico"]
			}
		} 

List of Multi Options (radio and multi-select drop-down) Question

	    	{
		    "id": "~13nacf",
		    "questionId": "2l23rao",
		    "questionText": "What is your favourite color?",
		    "value": {
			"@class": "list",
			"values": ["Red", "Blue"]
		    }
		}


**NPS Question Response**

Value for the nps type question

	     {
			"questionId" : "16qfkqk",
			"questionText" : "How likely is it that you would recommend WebEngage to a colleague?",
			"value" : {
			  "@class" : "score",
			  "value" : 10
			 }
	     } 



**Matrix Question Response**

Values for the matrix type questions.

	    	{
		    "id": "~vqxl96",
		    "questionId": "980gpa",
		    "questionText": "Rate our website",
		    "value": {
			"@class": "matrix",
			"values": {
			    "Experience": ["Good"],
			    "Content": ["Poor"],
			    "Design": ["Good"]
			}
		    }
		}

**File Question Response**

Values for the file uploads.
		{
		    "id": "~67j6bv",
		    "questionId": "2i98bsk",
		    "questionText": "Upload Product Image",
		    "value": {
		        "@class": "file",
		        "fileName": "shoes-for-men-by-tzaro.jpg",
		        "fileSize": 33731,
		        "fileAccessUrl": "https://api.webengage.com/v1/accounts/311c48b3/survey-responses/~67j6bv/download",
		        "fileType": "jpg"
		    }
		}
*Note* : Files can be downloaded by accessing the ``fileAccessUrl`` with api-credentials. Also you can make use of the ``fileName`` value to save the downloaded file like this

	curl -H "Authorization: bearer your_api_token" -o shoes-for-men-by-tzaro.jpg https://api.webengage.com/v1/accounts/311c48b3/survey-responses/~67j6bv/download
	
	  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
		                             Dload  Upload   Total   Spent    Left  Speed
	100 73623    0 73623    0     0  1630k      0 --:--:-- --:--:-- --:--:-- 1711k	


#### Survey Custom Data

Custom data fields that are submitted along with the survey on the publisher site.

	{
	    "customerType": ["Gold"],
	    "productCategory": ["Footwear"],
	    "jsessionid": ["CDB300FEF898236FF9E5A181E468CA6BCD"]
	}
