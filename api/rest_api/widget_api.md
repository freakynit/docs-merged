#### Widget Api

Widget or Account API. WebEngage allows trusted partners to create new WebEngage accuonts, we call it Widget Creation. Here is the sample Request and response of the same.

### Create WebEngage Widget

Create new WebEngage Account

	POST /v1/widget/create

#### POST parameters

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Parameter</th>
		<th>Type</th>
		<th>Description</th>
        <th>isMandatory</th>
	</tr>
	<tr>
		<td valign="top">email</td>
		<td valign="top">String</td>
		<td valign="top">Email address for the account registration, on which user will receive account activation email from WebEngage.</td>
		<td valign="top">Yes</td>
	</tr>
	<tr>
		<td valign="top">fullName</td>
		<td valign="top">String</td>
		<td valign="top">Full name of the user creating an account with WebEngage.</td>
		<td valign="top">No</td>
	</tr>
	<tr>
		<td valign="top">domain</td>
		<td valign="top">String</td>
		<td valign="top">Domain for which user is creating an account. User may log in and add multiple domain after account creation.</td>
		<td valign="top">No</td>
	</tr>
	<tr>
		<td valign="top">password</td>
		<td valign="top">String</td>
		<td valign="top">Password for the user account. User may not provide the password at the time of widget creation, instead he/she can set password at the time of account activation via verification email.</td>
		<td valign="top">No</td>		
	</tr>
</table>

#### JSON Response

	
	{
		"response": {
			"data": {
				"licenseCode": "~13410605b",
				"domain": "acme.com",
				"plan": "Free",
				"newAccount": true
            },
            "message": "A verification email has been sent to john@deo.com. Please follow the instructions therein to activate your WebEngage account.",
			"status": "success"
        }
	}

#### XML Respons

	<response>
		<status>success</status>
		<message>
			A verification email has been sent to john@deo.com. Please follow the instructions therein to activate your WebEngage account.
		</message>
		<data class="account">
			<licenseCode>~10a5cb9a9</licenseCode>
			<domain>acme.com</domain>
			<plan>Free</plan>
			<isNew>true</isNew>
		</data>
	</response>
