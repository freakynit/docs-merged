REST API
==============

Welcome to WebEngage REST API. This document covers how to use the WebEngage REST API. Using REST API, you can access WebEngage Feedback data, Survey questionnaire and its responses (coming soon) and Notification details and statistics (coming soon).

## Overview

* [API Schema](#schema)
* [Widget API](#widget)
* [Feedback API](#feedback)
* [Survey API](#survey)
* [Notification API](#) (coming soon)

## Schema

WebEngage API details.

### General Notes

**API HOST**

Access to all the resources available via API is over ``HTTPS`` and accessed from the host ``api.webengage.com``

	    https://api.webengage.com/
		
**Response Format**

All the data is sent in a ``JSON`` or ``XML`` format. ``format`` parameter can be passed in the URL as ``json`` or ``xml``. Default value is ``json``. (Any parameters can be passed as an HTTP query string parameter in the API Urls.)

**Date Format**

All timestamps are returned in the following format:

	    yyyy-MM-ddTHH:mm:ssZ
		
		e.g. 2013-01-26T07:31+0000

**Response Container**

Every API Response will be wrapped in a main container called ``response``. It will have three properties namely ``status``, ``data`` and ``message``. ``status`` will have values either ``success`` or ``error`` depending upon the server response. ``message`` will be related text for the corresponding ``status``. ``data`` will contain the data for the requested resource. It could be [Feedback Data](#get-a-feedback), [Feedback Reply Data](#get-a-feedback-reply) etc.

		{
			"response" : {
				"data" : {
					...
				},
				"message" : "success",
				"status" : "success"
			}
		}

### Authentication
WebEngage API follows "Bearer" Authentication Scheme for the API access.

**API KEY**

You can get your API key for an account from your dashboard. Every account manager for an account will have a different API Key. Access restriction for an API Key is driven by the access privileges of that account manager. You have to pass the API key in header, as shown below :

		curl -H "Authorization: bearer your_api_key" https://api.webengage.com/v1/feedback/has7e2 -d "format=xml"

### Errors

Following is the list of errors you may receive.


**Invalid Resource/Parameters**

Invalid Resource Id will result in : ``400 Bad Request``	    

	    {
			"response" : {
			"message" : "Invalid request",
			"status" : "error"
			}
		}

**Invalid URL**

Invalid URL will result in : ``404 Not Found``

	    {
			"response" : {
			"message" : "Invalid request",
			"status" : "error"
			}
		}

**Invalid Auth/Access**

Invalid authentication parameter will result in : ``401 Unauthorized``

	    {
			"response" : {
			"message" : "Authentication Required",
			"status" : "error"
			}
		}

## Widget 

Widget or Account API. WebEngage allows trusted partners to create new WebEngage accuonts, we call it Widget Creation. Here is the sample Request and response of the same.

### Create WebEngage Widget

Create new WebEngage Account

	POST /v1/widget/create

#### POST parameters

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Parameter</th>
		<th>Type</th>
		<th>Description</th>
        <th>isMandatory</th>
	</tr>
	<tr>
		<td valign="top">email</td>
		<td valign="top">String</td>
		<td valign="top">Email address for the account registration, on which user will receive account activation email from WebEngage.</td>
		<td valign="top">Yes</td>
	</tr>
	<tr>
		<td valign="top">fullName</td>
		<td valign="top">String</td>
		<td valign="top">Full name of the user creating an account with WebEngage.</td>
		<td valign="top">No</td>
	</tr>
	<tr>
		<td valign="top">domain</td>
		<td valign="top">String</td>
		<td valign="top">Domain for which user is creating an account. User may log in and add multiple domain after account creation.</td>
		<td valign="top">No</td>
	</tr>
	<tr>
		<td valign="top">password</td>
		<td valign="top">String</td>
		<td valign="top">Password for the user account. User may not provide the password at the time of widget creation, instead he/she can set password at the time of account activation via verification email.</td>
		<td valign="top">No</td>		
	</tr>
</table>

#### JSON Response

	
	{
		"response": {
			"data": {
				"licenseCode": "~13410605b",
				"domain": "acme.com",
				"plan": "Free",
				"newAccount": true
            },
            "message": "A verification email has been sent to john@deo.com. Please follow the instructions therein to activate your WebEngage account.",
			"status": "success"
        }
	}

#### XML Respons

	<response>
		<status>success</status>
		<message>
			A verification email has been sent to john@deo.com. Please follow the instructions therein to activate your WebEngage account.
		</message>
		<data class="account">
			<licenseCode>~10a5cb9a9</licenseCode>
			<domain>acme.com</domain>
			<plan>Free</plan>
			<isNew>true</isNew>
		</data>
	</response>

## Feedback

WebEngage allows you to query for the feedback response generated via WebEngage Feedback Widget using the **FeedbackId**. It also lets you query replies for a perticular feedback thread or even a specific reply using it's **ReplyId**.

### Get a Feedback

Get feedback details by **FeedbackId**.

	GET /v1/feedback/<FeedbackId>

#### XML Response

		<?xml version="1.0"?>
		<response>
				<data class="feedback">
				    <id>has7e2</id>
				    <licenseCode>311c48b3</licenseCode>
				    <activity>
				        <pageUrl>http://webengage.net/publisher/feedback-configuration/fields/311c48b3</pageUrl>
				        <pageTitle>Feedback Configuration - WebEngage</pageTitle>
				        <ip>127.0.0.1</ip>
				        <city>Mumbai</city>
				        <country>India</country>
				        <browser>Firefox</browser>
				        <browserVersion>18</browserVersion>
				        <platform>Linux</platform>
				        <activityOn>2013-02-11T08:09+0000</activityOn>
				    </activity>
				    <fields>
				        <field id="1fcdjgf" type="default">
				            <label>Name</label>
				            <value class="name">
				                <text>John</text>
				            </value>
				        </field>
				        <field id="ah1ihjd" type="default">
				            <label>Email</label>
				            <value class="email">
				                <text>john@doe.com</text>
				            </value>
				        </field>
				        <field id="cci1868" type="default">
				            <label>Category</label>
				            <value class="category">
				                <text>Suggestion</text>
				            </value>
				        </field>
				        <field id="~78me196" type="default">
				            <label>Message</label>
				            <value class="message">
				                <text>Thank you very much for this awesome service!</text>
				            </value>
				        </field>
				        <field id="~5d68amb" type="default">
				            <label>Attach a screenshot of this page</label>
				            <value class="snapshot">
				                <thumbnailImageUrl></thumbnailImageUrl>
				            </value>
				        </field>
				        <field id="n283q0" type="custom">
				            <label>Enter your mobile number</label>
				            <value class="text">
				                <text>9988776655</text>
				            </value>
				        </field>
					<field id="16qfkqk" type="custom">
						<label>"How likely is it that you would recommend WebEngage to a colleague?"</label>
						<value class="score">
							<value>10</value>
						</value>
					</field>
				        <field id="pp3j84" type="custom">
				            <label>Your Bio</label>
				            <value class="text">
				                <text>I am an early beta user of this product.</text>
				            </value>
				        </field>
				        <field id="19jb68o" type="custom">
				            <label>Which countries you have been to?</label>
				            <value class="list">
				                <options>
				                    <option>US</option>
				                    <option>Mexico</option>
				                </options>
				            </value>
				        </field>
				        <field id="1cc6lks" type="custom">
				            <label>Rate our website</label>
				            <value class="matrix">
				                <entries>
				                    <entry>
				                        <row>Experience</row>
				                        <columns>
				                            <column>Good</column>
				                        </columns>
				                    </entry>
				                    <entry>
				                        <row>Content</row>
				                        <columns>
				                            <column>Good</column>
				                        </columns>
				                    </entry>
				                    <entry>
				                        <row>Design</row>
				                        <columns>
				                            <column>Poor</column>
				                        </columns>
				                    </entry>
				                </entries>
				            </value>
				        </field>
				        <field id="sht4k8" type="custom">
				            <label>Upload your resume</label>
				            <value class="file">
				                <fileName>android_sdk_developing.pdf</fileName>
				                <fileSize>1919972</fileSize>
				                <fileAccessUrl>https://api.webengage.com/v1/accounts/311c48b3/feedback-responses/ofq4jy/download</fileAccessUrl>
				                <fileType>pdf</fileType>
				            </value>
				        </field>
				        <field id="16qfkqk" type="custom">
				            <label>What is your favourite color?</label>
				            <value class="list">
				                <options>
				                    <option>Red</option>
				                </options>
				            </value>
				        </field>
				    </fields>
					<customData>
						<entries>
							<entry>
								<key>userName</key>
								<values>
									<value>john</value>
								</values>
							</entry>
							<entry>
								<key>gender</key>
								<values>
									<value>male</value>
								</values>
							</entry>
							<entry>
								<key>customerType</key>
								<values>
									<value>Gold</value>
								</values>
							</entry>
							<entry>
								<key>jsessionid</key>
								<values>
									<value>CDB300FEF898236FF9E5A181E468CA6BCD</value>
								</values>
							</entry>							
						</entries>
					</customData>						
				    <replies>
				        <reply replyType="ADMIN">
				            <id>8bea242d</id>
				            <repliedByEmail>mike@company.com</repliedByEmail>
				            <repliedByName>Mike</repliedByName>
				            <repliedOn>2013-02-11T08:27+0000</repliedOn>
				            <repliedText>Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.

		Regards,
		Mike</repliedText>
				            <attachments></attachments>
				        </reply>
				        <reply replyType="USER">
				            <id>a5828a9e</id>
				            <repliedByEmail>john@doe.com</repliedByEmail>
				            <repliedByName>john</repliedByName>
				            <repliedOn>2013-02-11T08:37+0000</repliedOn>
				            <repliedText>Thanks! looking forward for the release.</repliedText>
				            <attachments></attachments>
				        </reply>
				        <reply replyType="ADMIN">
				            <id>be1a0220</id>
				            <repliedByEmail>mike@company.com</repliedByEmail>
				            <repliedByName>Mike</repliedByName>
				            <repliedOn>2013-02-11T08:39+0000</repliedOn>
				            <repliedText>Sure. Meanwhile, take a look at the attached screenshot of the first draft. </repliedText>
				            <attachments>
				                <attachment>
				                    <fileName>git.from.bottom.up.pdf</fileName>
				                    <fileSize>811094</fileSize>
				                    <fileAccessUrl>https://api.webengage.com/v1/accounts/311c48b3/feedback-reply-attachments/76178a43a/download</fileAccessUrl>
				                    <fileType>pdf</fileType>
				                </attachment>
				            </attachments>
				        </reply>
				    </replies>
				</data>
				<message>success</message>
				<status>success</status>
		</response>

#### JSON Response

	{
			"response" : {
				"data" : {
				  "licenseCode" : "311c48b3",
				  "fields" : [ {
				    "id" : "1fcdjgf",
				    "label" : "Name",
				    "type" : "default",
				    "value" : {
				      "@class" : "name",
				      "text" : "John"
				    }
				  }, {
				    "id" : "ah1ihjd",
				    "label" : "Email",
				    "type" : "default",
				    "value" : {
				      "@class" : "email",
				      "text" : "john@doe.com"
				    }
				  }, {
				    "id" : "cci1868",
				    "label" : "Category",
				    "type" : "default",
				    "value" : {
				      "@class" : "category",
				      "text" : "Suggestion"
				    }
				  }, {
				    "id" : "~78me196",
				    "label" : "Message",
				    "type" : "default",
				    "value" : {
				      "@class" : "message",
				      "text" : "Thank you very much for this awesome service!"
				    }
				  }, {
				    "id" : "~5d68amb",
				    "label" : "Attach a screenshot of this page",
				    "type" : "default",
				    "value" : {
				      "@class" : "snapshot",
				      "thumbnailImageUrl" : ""
				    }
				  }, {
				    "id" : "n283q0",
				    "label" : "Enter your mobile number",
				    "type" : "custom",
				    "value" : {
				      "@class" : "text",
				      "text" : "9988776655"
				    }
				  }, {
				    "id" : "pp3j84",
				    "label" : "Your Bio",
				    "type" : "custom",
				    "value" : {
				      "@class" : "text",
				      "text" : "I am an early beta user of this product."
				    }
				  }, {
				    "id" : "19jb68o",
				    "label" : "Which countries you have been to?",
				    "type" : "custom",
				    "value" : {
				      "@class" : "list",
				      "values" : [ "US", "Mexico" ]
				    }
				  }, {
				    "id" : "1cc6lks",
				    "label" : "Rate our website",
				    "type" : "custom",
				    "value" : {
				      "@class" : "matrix",
				      "values" : {
				        "Experience" : [ "Good" ],
				        "Content" : [ "Good" ],
				        "Design" : [ "Poor" ]
				      }
				    }
				  }, {
				    "id" : "sht4k8",
				    "label" : "Upload your resume",
				    "type" : "custom",
				    "value" : {
				      "@class" : "file",
				      "fileName" : "android_sdk_developing.pdf",
				      "fileSize" : 1919972,
				      "fileAccessUrl" : "https://api.webengage.com/v1/accounts/311c48b3/feedback-responses/ofq4jy/download",
				      "fileType" : "pdf"
				    }
				  }, {
				    "id" : "16qfkqk",
				    "label" : "What is your favourite color?",
				    "type" : "custom",
				    "value" : {
				      "@class" : "list",
				      "values" : [ "Red" ]
				    }
				  },{
				    "id" : "16qhkqk",
				    "label" : "How likely is it that you would recommend WebEngage to a colleague?",
				    "type" : "custom",
				    "value" : {
				      "@class" : "score",
				      "value" : 10
				    }
			          } ],
				  "customData" : {
					  "userName" : [ "john" ],
					  "gender" : [ "male" ],
					  "customerType" : [ "Gold" ],
					  "jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
				  },				  
				  "replies" : [ {
				    "repliedByEmail" : "mike@company.com",
				    "repliedByName" : "Mike",
				    "repliedOn" : "2013-02-11T08:27+0000",
				    "repliedText" : "Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.\r\n\r\nRegards,\r\nMike",
				    "attachments" : [ ],
				    "replyType" : "ADMIN",
				    "id" : "8bea242d"
				  }, {
				    "repliedByEmail" : "john@doe.com",
				    "repliedByName" : "john",
				    "repliedOn" : "2013-02-11T08:37+0000",
				    "repliedText" : "Thanks! looking forward for the release.",
				    "attachments" : [ ],
				    "replyType" : "USER",
				    "id" : "a5828a9e"
				  }, {
				    "repliedByEmail" : "mike@company.com",
				    "repliedByName" : "Mike",
				    "repliedOn" : "2013-02-11T08:39+0000",
				    "repliedText" : "Sure. Meanwhile, take a look at the attached screenshot of the first draft. ",
				    "attachments" : [ {
				      "fileName" : "git.from.bottom.up.pdf",
				      "fileSize" : 811094,
				      "fileAccessUrl" : "https://api.webengage.com/v1/accounts/311c48b3/feedback-reply-attachments/76178a43a/download",
				      "fileType" : "pdf"
				    } ],
				    "replyType" : "ADMIN",
				    "id" : "be1a0220"
				  } ],
				  "id" : "has7e2",
				  "activity" : {
				    "pageUrl" : "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
				    "pageTitle" : "Feedback Configuration - WebEngage",
				    "ip" : "127.0.0.1",
				    "city" : "Mumbai",
				    "country" : "India",
				    "browser" : "Firefox",
				    "browserVersion" : "18",
				    "platform" : "Linux",
				    "activityOn" : "2013-02-11T08:09+0000"
				  }
				},
				"message" : "success",
				"status" : "success"
			}
		}

#### User Activity

Details of the user taking feedback on the publisher website using WebEngage Feedback.

	{
		"pageUrl" : "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
		"pageTitle" : "Feedback Configuration - WebEngage",
		"ip" : "127.0.0.1",
		"city" : "Mumbai",
		"country" : "India",
		"browser" : "Firefox",
		"browserVersion" : "18",
		"platform" : "Linux",
		"activityOn" : "2013-02-11T08:09+0000"
	}

#### Form Fields

All the responses for the **Form Fields** of the feedback form are inside ``fields`` container. ``id`` specifies the field-id and type could be ``default`` (default fields present in the WebEngage feedback form) or ``custom`` (custom fields added in the form)

##### Default Form Fields

There are 5 types of **Default Fields** namely ``name``, ``email``, ``category``, ``message`` and ``snapshot``. ``class`` indicates the type of **Default Field**. Example of each is listed below. 

**Name Field**

	    {
			"id" : "1fcdjgf",
			"label" : "Name",
			"type" : "default",
			"value" : {
				"@class" : "name",
				"text" : "John"
			}
		}

**Email Field**

	    {
			"id" : "ah1ihjd",
			"label" : "Email",
			"type" : "default",
			"value" : {
				"@class" : "email",
				"text" : "john@doe.com"
			}
		}

**Category Field**

	    {
			"id" : "cci1868",
			"label" : "Category",
			"type" : "default",
			"value" : {
				"@class" : "category",
				"text" : "Suggestion"
			}
		}

**Message Field**

	    {
			"id" : "~78me196",
			"label" : "Message",
			"type" : "default",
			"value" : {
				"@class" : "message",
				"text" : "Thank you very much for this awesome service!"
			}
		}

**Snapshot Field**

	    {
			"id" : "~5d68amb",
			"label" : "Attach a screenshot of this page",
			"type" : "default",
			"value" : {
				"@class" : "snapshot",
				"thumbnailImageUrl" : "http://s3-ap-southeast-1.amazonaws.com/wk-static-files/feedback/82617417/1358664682292-095c-ed2f-06bb-thumb-small.jpg",
				"fullImageUrl" : "http://s3-ap-southeast-1.amazonaws.com/wk-static-files/feedback/82617417/1358664682292-095c-ed2f-06bb-full.jpg"
			}
		}

*Note* : ``thumbnailImageUrl`` and ``fullImageUrl`` are the public urls of the screenshot of the page, on which feedback was submitted.


##### Custom Form Fields

There are 4 types of **Custom Fields** namely ``list``, ``matrix``, ``file`` and ``text``. ``class`` indicates the type of **Custom Field**. Example of each type is listed below.

**Text Field**

Simple text field

	    {
			"id" : "n283q0",
			"label" : "Enter your mobile number",
			"type" : "custom",
			"value" : {
				"@class" : "text",
				"text" : "9988776655"
			}
		}

Simple comment field

	    {
			"id" : "pp3j84",
			"label" : "Your Bio",
			"type" : "custom",
			"value" : {
				"@class" : "text",
				"text" : "I am an early beta user of this product."
			}
		}

**List Field**

Single Options (radio and single-select drop-down) Fields

        {
			"id" : "16qfkqk",
			"label" : "What is your favourite color?",
			"type" : "custom",
			"value" : {
				"@class" : "list",
				"values" : [ "Red" ]
			}
		} 

List of Multi Options (radio and multi-select drop-down) Fields

	    {
			"id" : "19jb68o",
			"label" : "Which countries you have been to?",
			"type" : "custom",
			"value" : {
				"@class" : "list",
				"values" : [ "US", "Mexico" ]
			}
		}

**Matrix Field**

Values for the matrix type questions.

	    {
			"id" : "1cc6lks",
			"label" : "Rate our website",
			"type" : "custom",
			"value" : {
				"@class" : "matrix",
				"values" : {
					"Experience" : [ "Good" ],
					"Content" : [ "Good" ],
					"Design" : [ "Poor" ]
				}
			}
		}

**File Field**

Values for the file uploads.

	    {
			"id" : "sht4k8",
			"label" : "Upload your resume",
			"type" : "custom",
			"value" : {
				"@class" : "file",
				"fileName" : "android_sdk_developing.pdf",
				"fileSize" : 1919972,
				"fileAccessUrl" : "https://api.webengage.com/v1/accounts/311c48b3/feedback-responses/ofq4jy/download",
				"fileType" : "pdf"
			}
		}

*Note* : Files can be downloaded by accessing the ``fileAccessUrl`` with api-credentials. Also you can make use of the ``fileName`` value to save the downloaded file like this

	curl -H "Authorization: bearer your_api_token" -o android_sdk_developing.pdf https://api.webengage.com/v1/accounts/311c48b3/feedback-responses/ofq4jy/download
	
	  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
		                             Dload  Upload   Total   Spent    Left  Speed
	100 73623    0 73623    0     0  1630k      0 --:--:-- --:--:-- --:--:-- 1711k	


#### Custom Data

Custom data fields that are submitted along with the feedback on the publisher site.

	{
        "userName" : [ "john" ],
		"gender" : [ "male" ],
        "customerType" : [ "Gold" ],
        "jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
    }

#### Replies

``replies`` holds the list of replies in a particular feedback thread. ``reply`` holds all the data related to a perticular reply including all the attachments. [Goto](#get-feedback-replies) to see more about feedback replies/reply.


### Get Feedback Replies

Get all feedback replies using **FeedbackId**


	GET /v1/feedback/<FeedbackId>/replies


#### JSON Response

First reply added from site owner (Mike).

		{
			"response" : {
					"data" :
					[ {
						  "repliedByEmail" : "mike@company.com",
						  "repliedByName" : "Mike",
						  "repliedOn" : "2013-02-11T08:27+0000",
						  "repliedText" : "Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.\r\n\r\nRegards,\r\nMike",
						  "attachments" : [ ],
						  "replyType" : "ADMIN",
						  "id" : "8bea242d"
					} ]
				}
		}

Subsequent reply from John gets appended in the same "replies"

			{
				"response" : {
						"data" :
					[ {
							"repliedByEmail" : "mike@company.com",
							"repliedByName" : "Mike",
							"repliedOn" : "2013-02-11T08:27+0000",
							"repliedText" : "Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.\r\n\r\nRegards,\r\nMike",
							"attachments" : [ ],
							"replyType" : "ADMIN",
							"id" : "8bea242d"
						}, {
							"repliedByEmail" : "john@doe.com",
							"repliedByName" : "john",
							"repliedOn" : "2013-02-11T08:37+0000",
							"repliedText" : "Thanks! looking forward for the release.",
							"attachments" : [ ],
							"replyType" : "USER",
							"id" : "a5828a9e"
						} ]
					}
			}

While replying, feedback manager or the user can add file attachments.

		{
			"response" : {
					"data" :
					[ {
								"repliedByEmail" : "mike@company.com",
								"repliedByName" : "Mike",
								"repliedOn" : "2013-02-11T08:39+0000",
								"repliedText" : "Sure. Meanwhile, take a look at the attached screenshot of the first draft. ",
								"attachments" : [ {
								  "fileName" : "pic-feautre-beta.jpg",
								  "fileSize" : 811094,
								  "fileAccessUrl" : "https://api.webengage.com/v1/accounts/311c48b3/feedback-reply-attachments/76178a43a/download",
								  "fileType" : "jpg"
								} ],
								"replyType" : "ADMIN",
								"id" : "be1a0220"
					} ]
				}
		}

*Note* : Attachments can be downloaded by accessing the ``fileAccessUrl`` with api-credentials. Also you can make use of the ``fileName`` value to save the downloaded file like this

	curl -H "Authorization: bearer your_api_token" -o pic-feautre-beta.jpg https://api.webengage.com/v1/accounts/311c48b3/feedback-reply-attachments/76178a43a/download
	
	  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
		                             Dload  Upload   Total   Spent    Left  Speed
	100 73623    0 73623    0     0  1630k      0 --:--:-- --:--:-- --:--:-- 1711k	


### Get a Feedback Reply

Get individual feedback reply using **ReplyId**

	GET /v1/feedback/<FeedbackId>/reply/<ReplyId>

#### JSON Response

	{
			"response" : {
				"data" : {
				  "repliedByEmail" : "mike@company.com",
				  "repliedByName" : "Mike",
				  "repliedOn" : "2013-02-11T08:27+0000",
				  "repliedText" : "Thanks for your feedback Joe. Your feature request is under development and planned for this weekend's release.\r\n\r\nRegards,\r\nMike",
				  "attachments" : [ ],
				  "replyType" : "ADMIN",
				  "id" : "8bea242d"
				},
				"message" : "success",
				"status" : "success"
			}
		}

## Survey

WebEngage allows trusted partners to create a survey for an existing account and allows to query for the survey response generated via WebEngage Survey Widget using the **ResponseId**.

### Create a Survey

Create survey for a WebEngage account with licenseCode **LicenseCode**.

	/v1/survey/<LicenseCode>/create

#### Survey Create Input XML

	<?xml version="1.0" encoding="UTF-8"?>
	<request>
	   <data class="surveys">
	      <list>
	         <survey>
	            <title>first survey</title>
	            <description>descriptiom</description>
	            <startDate>2013-07-12T12:04:40+0000</startDate>
	            <endDate>2013-07-17T12:04:40+0000</endDate>
	            <responseChannel>widget</responseChannel>
	            <thanksMessage>Thank you for your time !</thanksMessage>
	            <questions>
	               <simpleChoiceQuestion>
	                  <text>Where do you live ?</text>
	                  <type>CHOICE</type>
	                  <mandatory>true</mandatory>
	                  <allowMultipleAnswers>true</allowMultipleAnswers>
	                  <addCommentBox>false</addCommentBox>
	                  <errorMessage>ERRRO</errorMessage>
	                  <choices>
	                     <string>place 1</string>
	                     <string>place 2</string>
	                     <string>place 3</string>
	                  </choices>
	               </simpleChoiceQuestion>
	               <simpleChoiceQuestion>
	                  <text>Your age range ?</text>
	                  <type>DROPDOWN</type>
	                  <mandatory>true</mandatory>
	                  <allowMultipleAnswers>false</allowMultipleAnswers>
	                  <addCommentBox>false</addCommentBox>
	                  <errorMessage>ERRRO!</errorMessage>
	                  <choices>
	                     <string>below 15</string>
	                     <string>between 15 and 40</string>
	                     <string>above 3</string>
	                  </choices>
	               </simpleChoiceQuestion>
	               <matrixChoiceQuestion>
	                  <text>Vehicles you user for the daily commute ?</text>
	                  <type>MATRIX_OF_CHOICE</type>
	                  <mandatory>false</mandatory>
	                  <allowMultipleAnswers>true</allowMultipleAnswers>
	                  <addCommentBox>true/addCommentBox>
	                  <errorMessage>ERRRO!</errorMessage>
	                  <rowChoices>
	                     <string>Private transport</string>
	                     <string>Local transport</string>
	                  </rowChoices>
	                  <columnChoices>
	                     <string>two wheeler</string>
	                     <string>car/taxi</string>
	                     <string>bus</string>
	                  </columnChoices>
	               </matrixChoiceQuestion>
	               <question>
	                  <text>Your name?</text>
	                  <type>TEXT</type>
	                  <mandatory>true</mandatory>
	                  <addCommentBox>false</addCommentBox>
	                  <errorMessage>ERRRO!</errorMessage>
	               </question>
	               <question>
	                  <text>Leave comment?</text>
	                  <type>COMMENT</type>
	                  <mandatory>true/false</mandatory>
	                  <errorMessage>ERRRO!</errorMessage>
	               </question>
	            </questions>
	         </survey>
	      </list>
	   </data>
	</request>
	
#### Survey Create Response XML

	<?xml version="1.0" encoding="UTF-8"?>
	 <response>    
	 	<message>Successfully created one survey for publisher domain johndeo.com</message>    
	 	<status>success</status>
	</response>

### Get a Survey Response

Get survey response details by **ResponseId**.

	GET /v1/survey/response/<ResponseId>

#### Survey XML Response

		<response>
			    <data class="surveyResponse">
				<id>1kfrn7c</id>
				<surveyId>7djl637</surveyId>
				<title>Customer Satisfaction Survey</title>
				<licenseCode>311c48b3</licenseCode>
				<activity>
				    <pageUrl>http://webengage.net/</pageUrl>
				    <pageTitle>In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage</pageTitle>
				    <ip>127.0.0.1</ip>
				    <city>Mumbai</city>
				    <country>India</country>
				    <browser>Chrome</browser>
				    <browserVersion>26</browserVersion>
				    <platform>Linux</platform>
				    <activityOn>2013-05-08T10:34:33+0000</activityOn>
				</activity>
				<questionResponses>
				    <questionResponse id="~11uth8m">
					<questionId>6f51d6</questionId>
					<questionText>Which countries you have been to?</questionText>
					<value class="list">
					    <options>
						<option>Mexico</option>
					    </options>
					</value>
				    </questionResponse>
				    <questionResponse id="~vqxl96">
					<questionId>980gpa</questionId>
					<questionText>Rate our website</questionText>
					<value class="matrix">
					    <entries>
						<entry>
						    <row>Experience</row>
						    <columns>
						        <column>Good</column>
						    </columns>
						</entry>
						<entry>
						    <row>Content</row>
						    <columns>
						        <column>Poor</column>
						    </columns>
						</entry>
						<entry>
						    <row>Design</row>
						    <columns>
						        <column>Good</column>
						    </columns>
						</entry>
					    </entries>
					</value>
				    </questionResponse>
				    <questionResponse id="~67j6bv">
					<questionId>2i98bsk</questionId>
					<questionText>Upload Product Image</questionText>
					<value class="file">
					    <fileName>shoes-for-men-by-tzaro.jpg</fileName>
					    <fileSize>33731</fileSize>
					    <fileAccessUrl>https://api.webengage.com/v1/accounts/311c48b3/survey-responses/~67j6bv/download</fileAccessUrl>
					    <fileType>jpg</fileType>
					</value>
				    </questionResponse>
				    <questionResponse id="~13nacf">
					<questionId>2l23rao</questionId>
					<questionText>What is your favourite color?</questionText>
					<value class="list">
					    <options>
						<option>Red</option>
						<option>Blue</option>
					    </options>
					</value>
				    </questionResponse>
				    <questionResponse id="ynhviv">
					<questionId>16qfkqk</questionId>
					<questionText>"How likely is it that you would recommend WebEngage to a colleague?"</questionText>
					<value class="score">
						<value>10</value>
					</value>
				    </questionResponse>
				    <questionResponse id="~gfaxas">
					<questionId>2notcms</questionId>
					<questionText>Your profile</questionText>
					<value class="map">
					    <entries>
						<entry>
						    <key>Name</key>
						    <value>jhon</value>
						</entry>
						<entry>
						    <key>Qualification</key>
						    <value/>
						</entry>
						<entry>
						    <key>Age</key>
						    <value>24</value>
						</entry>
					    </entries>
					</value>
				    </questionResponse>
				</questionResponses>
				<customData>
				    <entries>
					<entry>
					    <key>customerType</key>
					    <values>
						<value>Gold</value>
					    </values>
					</entry>
					<entry>
					    <key>productCategory</key>
					    <values>
						<value>Footwear</value>
					    </values>
					</entry>
					<entry>
					    <key>jsessionid</key>
					    <values>
						<value>CDB300FEF898236FF9E5A181E468CA6BCD</value>
					    </values>
					</entry>
				    </entries>
				</customData>
			    </data>
			    <message>success</message>
			    <status>success</status>
		</response>

#### Survey JSON Response

	{
		    "response": {
			"data": {
			    "surveyId": "7djl637",
			    "title": "Customer Satisfaction Survey",
			    "licenseCode": "311c48b3",
			    "questionResponses": [{
				    "id": "~11uth8m",
				    "questionId": "6f51d6",
				    "questionText": "Which countries you have been to?",
				    "value": {
				        "@class": "list",
				        "values": ["Mexico"]
				    }
				}, {
				    "id": "~vqxl96",
				    "questionId": "980gpa",
				    "questionText": "Rate our website",
				    "value": {
				        "@class": "matrix",
				        "values": {
				            "Experience": ["Good"],
				            "Content": ["Poor"],
				            "Design": ["Good"]
				        }
				    }
				}, {
				    "id": "~67j6bv",
				    "questionId": "2i98bsk",
				    "questionText": "Upload Product Image",
				    "value": {
				        "@class": "file",
				        "fileName": "shoes-for-men-by-tzaro.jpg",
				        "fileSize": 33731,
				        "fileAccessUrl": "https://api.webengage.com/v1/accounts/311c48b3/survey-responses/~67j6bv/download",
				        "fileType": "jpg"
				    }
				}, {
				    "id": "~13nacf",
				    "questionId": "2l23rao",
				    "questionText": "What is your favourite color?",
				    "value": {
				        "@class": "list",
				        "values": ["Red", "Blue"]
				    }
				}, {
				    "id": "~gfaxas",
				    "questionId": "2notcms",
				    "questionText": "Your profile",
				    "value": {
				        "@class": "map",
				        "values": {
				            "Name": "jhon",
				            "Qualification": "",
				            "Age": "24"
				        }
				    }
				}
			    ],
			    "customData": {
				"customerType": ["Gold"],
				"productCategory": ["Footwear"],
				"jsessionid": ["CDB300FEF898236FF9E5A181E468CA6BCD"]
			    },
			    "id": "1kfrn7c",
			    "activity": {
				"pageUrl": "http://webengage.net/",
				"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
				"ip": "127.0.0.1",
				"city": "Mumbai",
				"country": "India",
				"browser": "Chrome",
				"browserVersion": "26",
				"platform": "Linux",
				"activityOn": "2013-05-08T10:34:33+0000"
			    }
			},
			"message": "success",
			"status": "success"
		    }
	}
#### Survey User Activity

Details of the user taking feedback on the publisher website using WebEngage Feedback.

		{
		    "pageUrl": "http://webengage.net/",
		    "pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
		    "ip": "127.0.0.1",
		    "city": "Mumbai",
		    "country": "India",
		    "browser": "Chrome",
		    "browserVersion": "26",
		    "platform": "Linux",
		    "activityOn": "2013-05-08T10:34:33+0000"
		}

#### Survey Question Response

All the responses for the **survey question** are inside ``questionResponses`` container. ``id`` specifies the question_response_id.
There are 5 types of **question_type** namely ``list``, ``matrix``, ``file``, ``text`` and ``map``. ``class`` indicates the type of **question**. Example of each type is listed below.

**Text Question Response**

Simple text Question

	   	{
		    "id": "~gfaxas",
		    "questionId": "2notcms",
		    "questionText": "Your profile",
		    "value": {
			"@class": "map",
			"values": {
			    "Name": "jhon",
			    "Qualification": "",
			    "Age": "24"
			}
		    }
		}

**List Question Response**

Single Options (radio and single-select drop-down) Question

		{
			"id": "~11uth8m",
			"questionId": "6f51d6",
			"questionText": "Which countries you have been to?",
			"value": {
				"@class": "list",
				"values": ["Mexico"]
			}
		} 

List of Multi Options (radio and multi-select drop-down) Question

	    	{
		    "id": "~13nacf",
		    "questionId": "2l23rao",
		    "questionText": "What is your favourite color?",
		    "value": {
			"@class": "list",
			"values": ["Red", "Blue"]
		    }
		}


**NPS Question Response**

Value for the nps type question

	     {
			"questionId" : "16qfkqk",
			"questionText" : "How likely is it that you would recommend WebEngage to a colleague?",
			"value" : {
			  "@class" : "score",
			  "value" : 10
			 }
	     } 



**Matrix Question Response**

Values for the matrix type questions.

	    	{
		    "id": "~vqxl96",
		    "questionId": "980gpa",
		    "questionText": "Rate our website",
		    "value": {
			"@class": "matrix",
			"values": {
			    "Experience": ["Good"],
			    "Content": ["Poor"],
			    "Design": ["Good"]
			}
		    }
		}

**File Question Response**

Values for the file uploads.
		{
		    "id": "~67j6bv",
		    "questionId": "2i98bsk",
		    "questionText": "Upload Product Image",
		    "value": {
		        "@class": "file",
		        "fileName": "shoes-for-men-by-tzaro.jpg",
		        "fileSize": 33731,
		        "fileAccessUrl": "https://api.webengage.com/v1/accounts/311c48b3/survey-responses/~67j6bv/download",
		        "fileType": "jpg"
		    }
		}
*Note* : Files can be downloaded by accessing the ``fileAccessUrl`` with api-credentials. Also you can make use of the ``fileName`` value to save the downloaded file like this

	curl -H "Authorization: bearer your_api_token" -o shoes-for-men-by-tzaro.jpg https://api.webengage.com/v1/accounts/311c48b3/survey-responses/~67j6bv/download
	
	  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
		                             Dload  Upload   Total   Spent    Left  Speed
	100 73623    0 73623    0     0  1630k      0 --:--:-- --:--:-- --:--:-- 1711k	


#### Survey Custom Data

Custom data fields that are submitted along with the survey on the publisher site.

	{
	    "customerType": ["Gold"],
	    "productCategory": ["Footwear"],
	    "jsessionid": ["CDB300FEF898236FF9E5A181E468CA6BCD"]
	}
