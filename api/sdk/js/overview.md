#### Overview

Once you sign-up and integrate [Webengage Widget Code](http://webengage.com/integrate-with/your-website "Webengage Widget Code") on your web-site, you can use this Javascript API to change the default behaviour of how the Surveys, Feedback and Notifications work on your site to suit your needs.
