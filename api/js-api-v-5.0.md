# JavaScript API (Version 5.0)

Once you sign-up and integrate [Webengage Widget Code](http://webengage.com/integrate-with/your-website "Webengage Widget Code") on your web-site, you can use this Javascript API to change the default behaviour of how the Surveys, Feedback and Notifications work on your site to suit your needs.

## Overview

Jump to :

* [Widget Settings](#widget-settings)
	* [Properties](#widget-configuration-properties)
	* [Callbacks](#widget-callbacks)
	* [Methods](#global-methods)
* [Event Tracking](#event-tracking)
	* [Prameters](#parameters)
* [User Tracking](#user-tracking)
	* [methods](#user-methods)
* [Survey Settings](#survey-settings)
	* [Properties](#survey-configuration-properties)
	* [Callbacks](#survey-callbacks)
	* [Methods](#survey-methods)
* [Feedback Settings](#feedback-settings)
	* [Properties](#feedback-configuration-properties)
	* [Callbacks](#feedback-callbacks)
	* [Methods](#feedback-methods)
* [Notification Settings](#notification-settings)
	* [Properties](#notification-configuration-properties)
	* [Callbacks](#notification-callbacks)
	* [Methods](#notification-methods)


## Integration Code and API

### Default Integration Code

Following is the default **one time integration code** snippet -

	<script id="_webengage_script_tag" type="text/javascript">
		!function(e,t,n){function o(e,t){e[t[t.length-1]]=function(){r.__queue.push([t.join("."),arguments])}}var i,s,r=e[n],g=" ",l="init options track onReady".split(g),a="feedback survey notification".split(g),c="options render clear abort".split(g),p="Open Close Submit Complete View Click".split(g),u="identify login logout setAttribute".split(g);if(!r||!r.__v){for(e[n]=r={__queue:[],__v:"5.0",user:{}},i=0;i<l.length;i++)o(r,[l[i]]);for(i=0;i<a.length;i++){for(r[a[i]]={},s=0;s<c.length;s++)o(r[a[i]],[a[i],c[s]]);for(s=0;s<p.length;s++)o(r[a[i]],[a[i],"on"+p[s]])}for(i=0;i<u.length;i++)o(r.user,["user",u[i]]);var f=t.createElement("script"),d=t.getElementById("_webengage_script_tag");f.type="text/javascript",f.async=!0,f.src=("https:"==t.location.protocol?"https://ssl.widgets.webengage.com":"http://cdn.widgets.webengage.com")+"/js/widget/webengage-min-v-5.0.js",d.parentNode.insertBefore(f,d)}}(window,document,"webengage");

		webengage.init(_YOUR_LICENSE_CODE_);
	</script>

This code loads and initializes WebEngage Widget asynchronously, so it does not block loading of your web page. This is cautiously done to not impact page load time on your website. The URLs in the above code are **protocol relative**. This lets the browser to load the widget over the same protocol (HTTP or HTTPS) as the containing page, which will prevent "Insecure Content" warnings.

### Initialization

**webengage.init** function initialises the widget by providing the `licenseCode` of the account. Once the **webengage-widget** initialises, it waits for `DOMContentLoaded` event until it modifies the dom for loading further resources, making sure it does not block the `DOMContentLoaded`.


### Configuration (webengage options)

All global and static configuration either at the widget level or individual product (Feedback, Survey or Notification) level can be set with **options** method.


## Widget Settings

### Widget Configuration Properties

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">language* </td>
		<td valign="top">String</td>
		<td valign="top">Specify your widget language here. The static messages present in your feedback/survey form will appear in the language specified here.</td>
		<td valign="top">As per settings in the dashboard</td>
	</tr>
	<tr>
		<td valign="top">delay* </td>
		<td valign="top">number</td>
		<td valign="top">Delays API initialization for the time specified (in miliseconds).</td>
		<td valign="top">0</td>
	</tr>
	<tr>
		<td valign="top">defaultRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to false, then by default on widget initialization, none of the products (feedback, survey and notificaton) appears. To stop rendering selective products from rendering, set the **defaultRender** at the product level configuration properties. </td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">customData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your custom data here in proper JSON format to submit along with every survey / feedback submission and notification click</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">ruleData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your rule data here in proper JSON format. The keys should match with the values used in Targeting rulese section for the corresponding Survey or Notification</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">tokens</td>
		<td valign="top">object</td>
		<td valign="top">Specify your tokens data here in proper JSON format.</td>
		<td valign="top">null</td>
	</tr>
</table>

** Property used only at the init time, changing this property later will have no effect*

#### Examples of using Widget Configuration Properties

##### Load WebEngage widget after 5 seconds

	webengage.options('delay', 5);

##### Pass your own custom/business data for tracking (e.g. userId, email, sessionId etc.)

	webengage.options('customData', { 'userId' : '2947APW09', 'age' : 21, 'isPaidCust' : false });

This data will be passed to all the activated products Survey,Feedback and Notification unless their own specific custom data (eg. `webengage.feedback.options('customData', { 'userId' : '2947APW09', 'age' : 21, 'isPaidCust' : false })` ) is provided.


##### Hide WebEngage by default on mobile and tablets

	var isMobile = {
		Android: function() {
				return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	if (isMobile.any()) {
		webengage.options('defaultRender', false);
	}

### Widget Callbacks

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Callback</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">onReady*</td>
		<td valign="top">function</td>
		<td valign="top">Any code that you want to run after the API is loaded and initialized should be placed within the <tt class="docutils literal"><span class="pre">webengage.onReady</span></tt></td>
		<td valign="top">null</td>
	</tr>
</table>

#### Examples of using Widget Callback



##### Show a survey on click of a button instead of default rendering

	//default rendering is disabled
	webengage.survey.options('defaultRender', false);
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.survey.render();
		};
	});

## Event Tracking

Event tracking enables you to track data and trigger event on the basis of which one can using the following method.

### Event Method

**webengage.track**

This method allows you to pass business events to WebEngage for analytics or to trigger surveys/notifications.

All passed events are captured in the event analytics screen (coming soon)

#### Parameters
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">eventName</td>
		<td valign="top">String</td>
		<td valign="top">Name of the event that occurred</td>
	</tr>
	<tr>
		<td valign="top">eventData</td>
		<td valign="top">Object</td>
		<td valign="top">Data to associate with the event. Pass a simple object with only strings, numbers, booleans and dates as property values</td>
	</tr>
</table>

#### Example of a webengage.track call

##### Let WebEngage know when a visitor leaves a cart before checkout

	webengage.track('Added To Cart', { productId: 1337, product: 'Givenchy Pour Homme Cologne', category: 'Fragrance', price: 39.80, currency: 'USD', quantity: 1});


## User Tracking

User tracking API's lets you to attach attributes to logged in as well as anonymous users. Attributes are set of properties that generates user profiles. This profile information can be used to segment users, which can be then used for better targeting and for getting user analytics in the WebEngage Dashboard.

### User tracking Method

**webengage.user.identify**

This method lets you inform WebEngage about identified user and accepts user-idetification id as the input.

#### Parameters
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">userId</td>
		<td valign="top">String</td>
		<td valign="top">User identifier you want to track with WebEngage</td>
	</tr>
</table>

**webengage.user.login**

This method like **identify** method, lets you inform WebEngage about logged in user and accepts user-idetification id as the input.

#### Parameters
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">userId</td>
		<td valign="top">String</td>
		<td valign="top">User identifier you want to track with WebEngage</td>
	</tr>
</table>

**webengage.user.logout**

This method lets you inform WebEngage that current user has logged out and treat the use as anonymous user from this point onwards. This method does not expect any parameters.

**webengage.user.setAttribute**

This method lets you set user attributes for a logged-in user. This is useful when setting targeting rules for Surveys/Notification in the dasbhoard and also useful for user/segment analytics. This method accepts **key** `attribute-name` and **value** `attribute-value` as parameter and it also accetps an **object** of `attribute-name` and `attribute-value` as input.

#### Parameters when setting one attribute
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">attribute-name</td>
		<td valign="top">String</td>
		<td valign="top">Name of the attribute</td>
	</tr>
	<tr>
		<td valign="top">attribute-value</td>
		<td valign="top">String</td>
		<td valign="top">Value of the attribute</td>
	</tr>
</table>

#### Parameters when setting multiple attributes
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">attribute-key-value-object</td>
		<td valign="top">Object</td>
		<td valign="top">Attribute key value object</td>
	</tr>
</table>


##### Predefined attribute keys

<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">email</td>
		<td valign="top">String</td>
		<td valign="top">User Email address</td>
	</tr>
	<tr>
		<td valign="top">birth_date</td>
		<td valign="top">String</td>
		<td valign="top">User's birth-date as `yyyy-mm-dd` format</td>
	</tr>
	<tr>
		<td valign="top">gender</td>
		<td valign="top">String</td>
		<td valign="top">User's gender `male`, `female` or `other`</td>
	</tr>
	<tr>
		<td valign="top">first_name</td>
		<td valign="top">String</td>
		<td valign="top">User's first name</td>
	</tr>
	<tr>
		<td valign="top">last_name</td>
		<td valign="top">String</td>
		<td valign="top">User's last name</td>
	</tr>
	<tr>
		<td valign="top">company</td>
		<td valign="top">String</td>
		<td valign="top">User's company</td>
	</tr>
</table>



## Survey Settings
### Survey Configuration Properties

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">surveyId</td>
		<td valign="top">String</td>
		<td valign="top">To show a specific survey, specify the surveyId</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">skipRules</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, targeting rules specified in the dashboard for the active surveys are not executed. Thus, all the active surveys, unless surveyId is specified, become eligible to be shown.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">forcedRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, the submitted or closed surveys also become elgible to be shown.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">defaultRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to false, the eligible survey is not displayed by default.</td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">delay</td>
		<td valign="top">number</td>
		<td valign="top">Time delay, in miliseconds, to show a survey on a page. It overrides the time delay specified in the dashboard.</td>
		<td valign="top">As set in the dashboard</td>
	</tr>
	<tr>
		<td valign="top">scope</td>
		<td valign="top">string/object</td>
		<td valign="top">A visitor life cycle depends on a long term cookie installed for your site in a browser. Lifecyle of a survey depends on the scope of a visitor. If a survey is closed, it doesn't appear for the visitor in the same browser session. If a survey is submitted, then it doesn't appear for the same visitor at all. If you want a survey to appear in every new browser session irrespective of the survey being taken in a past session, you can define your own scope. In this case, specify scope as SESSION_ID and the lifecycle of a survey remains within the scope of a session id. See examples below - </td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">scopeType</td>
		<td valign="top">string</td>
		<td valign="top">By defining custom scope, you can make a survey submitted once in that scope. By specifying the scopeType as 'session', you can narrow a scope within that session, making a possibility of a survey appearing multiple times to a visitor per each scope. By specifying the scopeType as 'global', you can make a survey submitted once per each scope value across different browser sessions/visitors.</td>
		<td valign="top">session</td>
	</tr>
	<tr>
		<td valign="top">customData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your custom data for survey in proper JSON format to submit along with every survey submission. If not set then <tt class="docutils literal"><span class="pre">webengage.customData</span></tt> is referred</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">ruleData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your rule data here in proper JSON format to filter you audience, with the keys you specified in the WebEngage Dashboard while editing rule for the corresponding Survey. If not specified <tt class="docutils literal"><span class="pre">webengage.ruleData</span></tt> is refered</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">enableCallbacks</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, will enable callbacks for all the survey events.</td>
		<td valign="top">false</td>
	</tr>
</table>

#### Examples of using Survey Properties

##### Pass business data for custom rules

	webengage.survey.options('ruleData', {
		'itemsInCart' : 3,
		'cartAmount' : 1288,
		'customerType' : 'gold'
	});

##### Show surveys to a visitor everytime a new browser session starts
By specifying a session_id (some unique identifier for a browser session) as the survey scope with scopeType as 'session', one can make a survey re-appear to a visitor, even if (s)he has closed or submitted it in a previous browser session.

	webengage.survey.options({
		'scope' : '_USER_SESSION_ID_',
		'scopeType' : 'session'
	});

**Note** : Notice that **options** method either takes key value as input or javascript map.


##### Show a survey to a visitor every day irrespective (s)he has closed/submitted the same survey.
By specifying a today's date as the survey scope, one can make a survey re-appear to a visitor each day even if he has closed or submitted it.

	var today = new Date();
	webengage.survey.options('scope', {
		'scope' : (today.getDate()+"-"+today.getMonth()+"-"+today.getYear()),
		'scopeType' : 'session',
		'surveyIds' : ["~29aj48l"]
	});

##### Show a survey once to a logged in user from differernt browsers
If one wants a survey to be submitted once per logged in user irrespective of different browser sessions, then specify logged in user's email or userId as the scope with scopeType as 'global'.

	webengage.survey.options({
		'scope' : '_USER_EMAIL_',
		'scopeType' : 'global'
	});


### Survey Callbacks

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Callback</th>
		<th>Description</th>
		<th>Callback Data</th>
	</tr>
	<tr>
		<td valign="top">onOpen</td>
		<td valign="top">invoked on survey open</td>
		<td valign="top">Object</td>
	</tr>
	<tr>
		<td valign="top">onClose</td>
		<td valign="top">invoked when a survey is closed</td>
		<td valign="top">Object</td>
	</tr>
	<tr>
		<td valign="top">onSubmit</td>
		<td valign="top">invoked when a question is answered</td>
		<td valign="top">Object</td>
	</tr>
	<tr>
		<td valign="top">onComplete</td>
		<td valign="top">invoked when the thank you message is displayed in the end</td>
		<td valign="top">Object</td>
	</tr>
</table>

#### Survey onOpen callback data (JSON)

	{
			"surveyId": "~5g1c4fd",
			"licenseCode": "311c48b3",
			"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
			"type": "survey",
			"title": "Survey #1",
			"totalQuestions": 6,
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

#### Survey onSubmit callback data (JSON)

	{
			"surveyId": "~5g1c4fd",
			"licenseCode": "311c48b3",
			"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
			"type": "survey",
			"title": "Survey #1",
			"totalQuestions": 6,
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			},
			"questionResponses": [{
					"questionId": "~1fstl7a",
					"questionText": "What is your favourite color?",
					"value": {
							"@class": "list",
							"values": ["Red"]
					}
			}]
	}

#### Survey onComplete callback data (JSON)

	{
			"surveyId": "~5g1c4fd",
			"licenseCode": "311c48b3",
			"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
			"type": "survey",
			"title": "Survey #1",
			"totalQuestions": 6,
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			},
			"questionResponses": [
			{
					"questionId": "~1fstl7a",
					"questionText": "Your name?",
					"value": {
							"@class": "map",
							"values": {
									"First name": "ABC",
									"Last name": "EFG"
							}
					}
			},
			{
					"questionId": "~1asf233",
					"questionText": "Enter your mobile number?",
					"value": {
							"@class": "text",
							"text": "9988776655"
					}
			},
			{
					"questionId": "we2e4545",
					"questionText": "Your Bio?",
					"value": {
							"@class": "text",
							"text" : "I am an early beta user of this product."
					}
			},
			{
					"questionId": "324saf3w",
					"questionText": "What is your favourite color?",
					"value": {
							"@class": "list",
							"values": ["Red"]
					}
			},
			{
					"questionId": "~213wrw43s",
					"questionText": "Which countries you have been to?",
					"value": {
							"@class": "list",
							"values": [ "US", "Mexico" ]
					}
			},
			{
					"questionId": "~ew347fev"
					"questionText": "Rate out website?",
					"value": {
							"@class" : "matrix",
							"values" : {
									"Experience" : [ "Good" ],
									"Content" : [ "Good" ],
									"Design" : [ "Poor" ]
							}
					}
			},
			{
					"questionId": "sht4k8",
					"questionText": "Upload your resume?",
					"value": {
							"@class" : "file",
							"fileName" : "android_sdk_developing.pdf",
							"fileSize" : 1919972,
							"fileAccessUrl" : "https://api.webengage.com/v1/download/feedback/response/ofq4jy",
							"fileType" : "pdf"
					}
			}]
	}

#### Survey onClose callback data (JSON)
On survey close the callback data for close event handlers will the latest event's (Open, Submit or Complete) callback data that happened before close.
Lets say if you close a survey after it opens the callback data for close event would same as the onOpen callback data as mentioned above.

	{
			"surveyId": "~5g1c4fd",
			"licenseCode": "311c48b3",
			"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
			"type": "survey",
			"title": "Survey #1",
			"totalQuestions": 6,
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

#### Examples of using Survey Callbacks

##### Close survey after certain response of a particular question

	var _weq = _weq || {};
	_weq['webengage.licenseCode'] = '_LICENSE_CODE_';
	_weq['webengage.enableCallbacks'] = true;
	_weq['webengage.survey.onSubmit'] = function (data) {
		var surveyInstance = this;
		if(data !== undefined && data.questionResponses !== undefined && data.questionResponses.length > 0) {
			for(var i=0; i < data.questionResponses.length; i++) {
				var question = data.questionResponses[i];
				if(question["questionId"] === "324saf3w") {
					var questionResponse = question["value"];
					var values = questionResponse["values"];
					if(values !== 'undefined' &&  values.length > 0){
						for(var k=0; k < values.length; k++) {
							if(values[k] === 'RED') {
								surveyInstance.close();
								break;
							}
						}
					}
					break;
				}
			}
		}
	};

##### Redirect a user on survey completion

	webengage.survey.onComplete = function () {
		window.location.href='/newletter/subscribe.html';
	};

### Survey Methods

**webengage.survey.render**

This method allows you to overwrite the dashboard settings. It also overwrites the settings provided in [_weq](#global-map-_weq) in the widget code. This method accepts all the [_weq survey configuration properties](#survey-configuration-properties) and [_weq callbacks](#survey-callbacks).

<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">options</td>
		<td valign="top">Object</td>
		<td valign="top"><a href="#survey-configuration-properties">Survey configuration properties</a> &nbsp; <a href="#survey-callbacks">callbacks</a>
</td>
	</tr>
</table>

#### Examples of Render Method

##### Open a survey on click of a button

	webengage.survey.options({
		'surveyId' : '_SURVEY_ID_', // survey id to be rendered ... note : it is optional
		'defaultRender' : false
	});
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.survey.render();
		};
	});

<table style="margin: 0; padding: 0; border: none;" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="margin: 0; padding: 0; border: none;">
			<button id="v4-btn-open-survey" class="cupid-green">Pop A Survey</button>
		</td>
	</tr>
</table>

##### Open a survey on page scroll (down)

	webengage.survey.options({
		'defaultRender' : false
	});
	(function () {
		var getScrollPercentage = function () {
			var documentHeight = $(document).height();
			var viewPortHeight = $(window).height();
			var verticlePageOffset = $(document).scrollTop();
			return Math.round(100* (viewPortHeight+verticlePageOffset)/documentHeight);
		}
		var surveyDisplayed = false;
		webengage.onReady(function(){
			window.onscroll = function (event) {
				if (!surveyDisplayed && getScrollPercentage() >= 70) {
					webengage.survey.render({ surveyId : '_SURVEY_ID_' }); // invoking webengage.survey.render with specific survey id
					surveyDisplayed = true;
				}
		}
		});
	})();

Please scroll down, till the end to see this in effect.
Also, notice that we are calling `webengage.survey.render` by passing it surveyId option.

##### Passing Custom Data / Rule Data

	webengage.survey.options({
		'defaultRender' : false
	});
	webengage.survey.options('ruleData', { 'categoryCode' '_CATEGORY_CODE_', 'paidCustomer' : true });
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.survey.render({
				'customData' :  { 'userId' : '_USER_ID_', 'age' : _AGE_ }
			});
		};
	});



##### Track survey view in console

	webengage.survey.options({
		'defaultRender' : false
	});
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function(){
			webengage.survey.render().onOpen( function(){
				console.log("survey open");
			});
		};
	});

**webengage.survey.clear**

Clears rendered survey.

#### Examples of Clear Method

##### Clear a survey on click of a button

	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.survey.clear();
		};
	});

## Feedback Settings

### Feedback Configuration Properties

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">delay</td>
		<td valign="top">number</td>
		<td valign="top">Time delay, in miliseconds, to show the feedback tab on a page.</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">formData</td>
		<td valign="top">Array of <a href="#formdata-object">Object</a></td>
		<td valign="top">Specify values in the feedback form for the pre-population of the fields. <a href="#prepopulate-email-hide-message-select-category-and-keep-it-readonly">example</a></td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">alignment</td>
		<td valign="top">String</td>
		<td valign="top">Shows the feedback tab on the left/right side of the webpage. The possible value that you can specify here is <strong>left</strong> or <strong>right</strong>.</td>
		<td valign="top">As specified in feedback tab configuration</td>
	</tr>
	<tr>
		<td valign="top">borderColor</td>
		<td valign="top">String</td>
		<td valign="top">Renders feedback tab with the specified border color</td>
		<td valign="top">As specified in feedback tab configuration</td>
	</tr>
	<tr>
		<td valign="top">backgroundColor</td>
		<td valign="top">String</td>
		<td valign="top">Renders feedback tab with the specified background color</td>
		<td valign="top">As specified in feedback tab configuration</td>
	</tr>
	<tr>
		<td valign="top">defaultCategory</td>
		<td valign="top">String</td>
		<td valign="top">Shows the feedback form based on the feedback category specified here.</td>
		<td valign="top">As specified in feedback category configuration</td>
	</tr>
	<tr>
		<td valign="top">showAllCategories</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to <strong>true</strong>, the feedback form appears with feedback category dropdown menu to let the end users to submit contextual feedbacks. If set to <strong>false</strong>, the feedback form appears based on the default feedback category specified without any feedback category dropdown menu.</td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">showForm</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to <strong>true</strong>, feedback form slides out, on page load, by default.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">defaultRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to false, the feedback tab is not displayed by default.</td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">customData</td>
		<td valign="top">Object</td>
		<td valign="top">Specify your custom data here in proper JSON format to submit along with every feedback got submitted, if not specified <tt class="docutils literal"><span class="pre">webengage.data</span></tt> is referred.</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">enableCallbacks</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, will enable callbacks for all the feedback events.</td>
		<td valign="top">false</td>
	</tr>
</table>

#### formData Object

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	<tr>
		<td valign="top">name</td>
		<td valign="top">String</td>
		<td valign="top">Label of the field in the feedback form which you want to prepopulate data or want to hide or make it mandatory</td>
	</tr>
	<tr>
		<td valign="top">value</td>
		<td valign="top">String</td>
		<td valign="top">Spefy the value of the field</td>
	</tr>
	<tr>
		<td valign="top">mode</td>
		<td valign="top">String</td>
		<td valign="top">Specify if the field should be hidden, read only or default (no-change).  So values only supported are <tt class="docutils literal"><span class="pre">hidden</span></tt> and <tt class="docutils literal"><span class="pre">readOnly</span></tt> and <tt class="docutils literal"><span class="pre">default</span></tt></td>
	</tr>
	<tr>
		<td valign="top">isMandatory</td>
		<td valign="top">Boolean</td>
		<td valign="top">Specify if the field should be mandatory or not.</td>
	</tr>
	<tr>
		<td valign="top">options</td>
		<td valign="top">String Array</td>
		<td valign="top">Applicable only in case of 'Category' field. Specify in case you want to show specific values in the category dropdown.</td>
	</tr>
</table>

**NOTE** : To see the example code click [here](#prepopulate-email-hide-message-select-category-and-keep-it-readonly)

#### Examples of using Feedback Properties

##### Set feedbackTab alignment to right

	webengage.feedback.options('alignment', 'right');

##### Set feedbackTab background colours and border colours

	webengage.feedback.options({
		'backgroundColor' : '#ff9',
		'borderColor' : '#f99'
	});


##### Prepopulate email and keep it readonly, hide message, specific options for category drop-down
	// Values int the category drop-down 'Like' & 'Question'

	webengage.feedback.options('formData', [
		{
			'name' : 'email',
			'value' : '_EMAIL_VALUE_',
			'mode' : 'readOnly'
		}, {
			'name' : 'message',
			'mode' : 'hidden'
		}, {
			'name' : 'category',
			'isMandatory' : false,
			'value' : 'like',
			'options' : ['Like', 'Question'] // make sure you send right category lables
		}
	]);

<table style="margin: 0; padding: 0; border: none;" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="margin: 0; padding: 0; border: none;">
			<button id="v4-btn-feedback-pre-populated" class="cupid-green">Prepopulate Feedback Fields</button>
		</td>
	</tr>
</table>


### Feedback Callbacks

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Callback</th>
		<th>Description</th>
		<th>Data</th>
	</tr>
	<tr>
		<td valign="top">onOpen</td>
		<td valign="top">invoked on survey open</td>
		<td valign="top">Data Object</td>
	</tr>
	<tr>
		<td valign="top">onClose</td>
		<td valign="top">invoked when a survey is closed</td>
		<td valign="top">Data Object</td>
	</tr>
	<tr>
		<td valign="top">onSubmit</td>
		<td valign="top">invoked when a question is answered</td>
		<td valign="top">Data Object</td>
	</tr>
</table>

####Feedback onOpen callback data (JSON)

	{
			"licenseCode": "311c48b3",
			"type": "feedback",
			"activity": {
			"pageUrl": "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
			"pageTitle": "Feedback Configuration - WebEngage",
			"ip": "127.0.0.1",
			"city": "Mumbai",
			"country": "India",
			"browser": "Firefox",
			"browserVersion": "18",
			"platform": "Linux",
			"activityOn": "2013-02-11T08:09+0000"
		},
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

####Feedback onSubmit callback data (JSON)

	{
		"id": "has7e2",
		"licenseCode": "311c48b3",
		"type": "feedback",
		"fields": [{
			"id": "1fcdjgf",
			"label": "Name",
			"type": "default",
			"value": {
				"@class": "name",
				"text": "John"
			}
		}, {
			"id": "ah1ihjd",
			"label": "Email",
			"type": "default",
			"value": {
				"@class": "email",
				"text": "john@doe.com"
			}
		}, {
			"id": "cci1868",
			"label": "Category",
			"type": "default",
			"value": {
				"@class": "category",
				"text": "Suggestion"
			}
		}, {
			"id": "~78me196",
			"label": "Message",
			"type": "default",
			"value": {
				"@class": "message",
				"text": "Thank you very much for this awesome service!"
			}
		}, {
			"id": "~5d68amb",
			"label": "Attach a screenshot of this page",
			"type": "default",
			"value": {
				"@class": "snapshot",
				"thumbnailImageUrl": ""
			}
		}, {
			"id": "n283q0",
			"label": "Enter your mobile number",
			"type": "custom",
			"value": {
				"@class": "text",
				"text": "9988776655"
			}
		}, {
			"id": "pp3j84",
			"label": "Your Bio",
			"type": "custom",
			"value": {
				"@class": "text",
				"text": "I am an early beta user of this product."
			}
		}, {
			"id": "19jb68o",
			"label": "Which countries you have been to?",
			"type": "custom",
			"value": {
				"@class": "list",
				"values": ["US", "Mexico"]
			}
		}, {
			"id": "1cc6lks",
			"label": "Rate our website",
			"type": "custom",
			"value": {
				"@class": "matrix",
				"values": {
					"Experience": ["Good"],
					"Content": ["Good"],
					"Design": ["Poor"]
				}
			}
		}, {
			"id": "sht4k8",
			"label": "Upload your resume",
			"type": "custom",
			"value": {
				"@class": "file",
				"fileName": "android_sdk_developing.pdf",
				"fileSize": 1919972,
				"fileAccessUrl": "https://api.webengage.com/v1/download/feedback/response/ofq4jy",
				"fileType": "pdf"
			}
		}, {
			"id": "16qfkqk",
			"label": "What is your favourite color?",
			"type": "custom",
			"value": {
				"@class": "list",
				"values": ["Red"]
			}
		}],
		"activity": {
			"pageUrl": "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
			"pageTitle": "Feedback Configuration - WebEngage",
			"ip": "127.0.0.1",
			"city": "Mumbai",
			"country": "India",
			"browser": "Firefox",
			"browserVersion": "18",
			"platform": "Linux",
			"activityOn": "2013-02-11T08:09+0000"
		},
		"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}
####Feedback onClose callback data (JSON)

On feedback close the callback data for close event handlers will the latest event's (Open or Submit) callback data that happened before close.
Lets say if you close feedback window after it opens the callback data for close event would same as the onOpen callback data as mentioned above.

	{
			"licenseCode": "311c48b3",
			"type": "feedback",
			"activity": {
			"pageUrl": "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
			"pageTitle": "Feedback Configuration - WebEngage",
			"ip": "127.0.0.1",
			"city": "Mumbai",
			"country": "India",
			"browser": "Firefox",
			"browserVersion": "18",
			"platform": "Linux",
			"activityOn": "2013-02-11T08:09+0000"
		},
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}


#### Examples of using Feedback Callbacks

##### Close the feedback window in 5 second just after someone submits feedback.

	webengage.feedback.onSubmit = function () {
			var feedbackInstance = this;
			setTimeout(function(){
				feedbackInstance.close();
			}, 5000);
	};

### Feedback Methods

**webengage.feedback.render**

This method allows you to overwrite the dashboard settings. It also overwrites the settings provided in [_weq](#global-map-_weq) in the widget code. This method accepts all the [_weq feedback configuration properties](#feedback-configuration-properties) and [_weq callbacks](#feedback-callbacks).


<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">options</td>
		<td valign="top">Object</td>
		<td valign="top"><a href="#feedback-configuration-properties">Feedback Configuration Properties</a> &amp;<a href="#feedback-callbacks">Callbacks</a>
		</td>
	</tr>
</table>

#### Examples for Render Method

##### Show a feedback on click of a button

	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.feedback.render({ 'showForm' : true });
		};
	});

<table style="margin: 0; padding: 0; border: none;" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="margin: 0; padding: 0; border: none;">
			<button id="v4-btn-open-feedback" class="cupid-green">Open Feedback Form</button>
		</td>
	</tr>
</table>

#### Example of clear Method

##### Clear a feedback on click of a button

	document.getElementById("_BUTTON_ID_").onclick = function () {
		webengage.feedback.clear();
	};


## Notification Settings

### Notification Configuration Properties

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">notificationId</td>
		<td valign="top">String</td>
		<td valign="top">To show a specific notification, specify the notificationId</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">skipRules</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, targeting rules specified in the dashboard for the active notifications are not executed. Thus, all the active notifications, unless notificationId is specified, become eligible to be shown.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">forcedRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, the clicked or closed notifications also become elgible to be shown.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">delay</td>
		<td valign="top">number</td>
		<td valign="top">Time delay, in miliseconds, to show a survey on a page. It overrides the time delay specified in the dashboard.</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">defaultRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to false, the eligible notification is not displayed by default.</td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">customData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your custom data for notification here in proper JSON format to submit along with every notification click. If not set then <tt class="docutils literal"><span class="pre">webengage.customData</span></tt> is referred</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">ruleData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your rule data here in proper JSON format to filter you audience, with the keys you specified in the WebEngage Dashboard while editing rule for the corresponding Notification. If not specified <tt class="docutils literal"><span class="pre">webengage.ruleData</span></tt> is refered</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">tokens</td>
		<td valign="top">object</td>
		<td valign="top">Specify your tokens data here in proper JSON format. If not specified <tt class="docutils literal"><span class="pre">webengage.tokens</span></tt> is refered</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">enableCallbacks</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, will enable callbacks for all the notification events.</td>
		<td valign="top">false</td>
	</tr>
</table>

#### Examples of using Notification Properties

##### Delay a particular notification rendering

	webengage.notification.options({
		'delay' : 5,
		'notificationId' : '_NOTIFICATION_ID_'
	});

**Note** : If you have already set the time-delay in the notification rule while creating it, `delay` value pased in the **webengage.notification.options** will overwrite it.

##### Skip rules on a page for notifications

	webengage.notification.options('skipRules', true);

##### Custom rules - showing a "we are offline" notification every Sunday

	var daysInWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	webengage.notification.options('ruleData', {day : daysInWeek[new Date().getDay()]});

**Note**: In custom rules section, on the targeting page, you can use the "day" variable to create targeting rules. For example, if the day is "Sunday", display a notification on your contact page that your support team is currently offline.

### Notification Callbacks

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Callback</th>
		<th>Description</th>
		<th>Data</th>
	</tr>
	<tr>
		<td valign="top">onOpen</td>
		<td valign="top">invoked on notification open</td>
		<td valign="top">Data Object</td>
	</tr>
	<tr>
		<td valign="top">onClose</td>
		<td valign="top">invoked when a notification is closed</td>
		<td valign="top">Data Object</td>
	</tr>
	<tr>
		<td valign="top">onClick</td>
		<td valign="top">invoked when a notification is clicked</td>
		<td valign="top">Data Object</td>
	</tr>
</table>

####Notification onOpen callback data (JSON)

	{
		"notificationId": "173049892",
		"licenseCode": "311c48b3",
		"type": "notification",
		"title": "Checkout the new pricing",
		"event": "open",
		"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
		"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

####Notification onClick callback data (JSON)

	{
		"notificationId": "173049892",
		"licenseCode": "311c48b3",
		"type": "notification",
		"title": "Checkout the new pricing",
		"event": "click",
		"actionLink": "http://webengage.com/pricing",
		"actionText": "Check out",
		"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
		"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

####Notification onClose callback data (JSON)

On notification close the callback data for close event handlers will the latest event's (Open or Click) callback data that happened before close.
Lets say if you close a notification after it opens the callback data for close event would same as the onOpen callback data as mentioned above.

	{
		"notificationId": "173049892",
		"licenseCode": "311c48b3",
		"type": "notification",
		"title": "Checkout the new pricing",
		"event": "click",
		"actionLink": "http://webengage.com/pricing",
		"actionText": "Check out",
		"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
		"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

#### Examples of using Notification Callbacks

##### Callback handler when a notification is closed

	webengage.notification.onClose = function () {
		var r=confirm("Do you realy want to miss this offer");
		if (r==false){
		  return false;
		}
	};

### Notification Methods

**webengage.notification.render**

This method allows you to overwrite the dashboard settings. It also overwrites the settings provided in [_weq](#global-map-_weq) in the widget code. This method accepts all the [_weq notification configuration properties](#notification-configuration-properties) and [_weq callbacks](#notification-callbacks).


<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">options</td>
		<td valign="top">Object</td>
		<td valign="top"><a href="#notification-configuration-properties">Notification configuration properties</a> &nbsp;<a href="#notification-callbacks">Callbacks</a>
		</td>
	</tr>
</table>

#### Examples of Render Method

##### Open a notification on click of a button

	webengage.notification.options('defaultRender', false);
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.notification.render();
		};
	});

<table style="margin: 0; padding: 0; border: none;" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="margin: 0; padding: 0; border: none;">
			<button id="v4-btn-open-notification" class="cupid-green">Pop A Notification</button>
		</td>
	</tr>
</table>

##### Open a notification on page scroll (down)

	webengage.notification.options('defaultRender', false);
	var getScrollPercentage = function () {
		var documentHeight = $(document).height();
		var viewPortHeight = $(window).height();
		var verticlePageOffset = $(document).scrollTop();
		return Math.round(100* (viewPortHeight+verticlePageOffset)/documentHeight);
	}
	var notificationDisplayed = false;
	webengage.onReady(function(){
		window.onscroll = function (event) {
			if (!notificationDisplayed && getScrollPercentage() >= 70) {
				webengage.notification.render({ notificationId : '_NOTIFICATION_ID_' }); // invoking webengage.notification.render with specific notification id
				notificationDisplayed = true;
			}
		};
	});

Please scroll down a bit to see this in effect.
Also, notice that we are calling `webengage.notification.render` by passing it notificationId option.

##### Passing Custom Data / Rule Data

	webengage.notification.options('defaultRender', false);
	webengage.notification.options('ruleData', { 'categoryCode' '_CATEGORY_CODE_', 'paidCustomer' : true });
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.notification.render({
				'customData' :  { 'userId' : '_USER_ID_', 'age' : _AGE_ }
			});
		};
	});


##### popping an alert onClose of a notification

	webengage.notification.options('defaultRender', false);
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function(){
			webengage.notification.render().onClose( function(){
				alert("You have closed the notification");
			});
		};
	});

**webengage.notification.clear**

Clears rendered notification.

#### Examples of Clear Method

##### Clear a notification on click of a button

	webengage.notification.options('defaultRender', false);
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.notification.clear();
		};
	});
